﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

// One script on each hand. Each refers to corresponding foot.
public class controllerFloat : MonoBehaviour
{    
    [Header("Settings")]
    [SerializeField]
    private float jetstrength;

    [Header("Refernces")]
    public LineRenderer laserVisual;
    public LayerMask laserMask;
    public LayerMask laserMaskMenuOnly;
    private int laserVisColor;
    [HideInInspector] public Animator anim;
    private Transform laserMuzzle;
    private fullBodyAssign fbtMgr;
    private Rigidbody rigid;

    public Transform hand;
    public Transform foot;
    public Transform hip;

    // refers to songManager's blockPrefabs array
    public bool isPauseMenu = false;
    public bool isPauseMenuPointer = false;

    Vector3 playerCenter;

    // steamvr stuff
    [Header("SteamVR INput")]
    private SteamVR_Behaviour_Pose trackedObj;
    public SteamVR_Action_Boolean button1 = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
    public SteamVR_Action_Boolean button2 = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabGrip");
    public SteamVR_Action_Boolean menu = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");
    public SteamVR_Action_Boolean padTouch = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("touchpadtouch");
    public SteamVR_Action_Boolean padClick = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("touchpadclick");
    public SteamVR_Action_Vector2 padPos = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("touchpadposition");
    public float padDeadzone = .35F;
    public bool reverseTouchpad = false;
    private int revPad = 1; // or -1

    private bool padClickedDown = false;
    private bool padTouched = false;
    public bool button1Down = false;
    private enum padZones {
        up=1,
        down=2,
        leftdown=4,
        leftup = 4,
        right =3,
        dead=0,
        none=-1,
    }
    private padZones padZone;
    private padZones oldPadZone;
    private Vector2 padDir;

    
    private trackerPower handPower;
    private trackerPower footPower;
    private trackerPower hipPower;

    public enum limbModes
    {
        none=0,
        grab=1,
        jet=2,
        shoot=3,
        more=4,
    }

    private limbModes handMode;
    private limbModes footMode;
    private bool handLocked;
    private bool footLocked;

    // Use this for initialization
    void Start()
    {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
        anim = GetComponentInChildren<Animator>();
        laserMuzzle = laserVisual.transform;
        playerCenter = new Vector3(0, 1, 0);
        fbtMgr = GetComponentInParent<fullBodyAssign>();

        handPower = GetComponent<trackerPower>();
        footPower = foot.GetComponent<trackerPower>();
        hipPower = hip.GetComponent<trackerPower>();
        // block visuals (editor)

        if (reverseTouchpad)
        {
            revPad = -1;
        }
    }

    
    // Update is called once per frame
    void Update()
    {
        // update padZone (where is thumb on the touchpad: SQUARE DEADZONE (fix with math TODO))
        if (padTouch.GetState(trackedObj.inputSource)) { 
            padDir = padPos.GetAxis(trackedObj.inputSource);
            padClickedDown = padClick.GetStateDown(trackedObj.inputSource);
            padTouched = true;
            oldPadZone = padZone;
            if(padDir.y > padDeadzone && (Mathf.Abs(padDir.x) < Mathf.Abs(padDir.y)))
            {
                padZone = padZones.up;
            }
            else if (padDir.y < -padDeadzone && (Mathf.Abs(padDir.x) < Mathf.Abs(padDir.y)))
            {
                padZone = padZones.down;
            }
            else if (padDir.x * revPad > padDeadzone && (Mathf.Abs(padDir.y) < Mathf.Abs(padDir.x)))
            {
                padZone = padZones.right;
            }
            else if (padDir.x * revPad < -padDeadzone && (Mathf.Abs(padDir.y) < Mathf.Abs(padDir.x)))
            {
                if(padDir.y > 0)
                {
                    padZone = padZones.leftup;
                }
                else
                {
                    padZone = padZones.leftdown;
                }
                
            }
            else if(Mathf.Abs(padDir.x)< padDeadzone && Mathf.Abs(padDir.y) < padDeadzone)
            {
                padZone = padZones.dead;
            }
            handMode = (limbModes) padZone;
        }
        else
        {
        }

        // Spawn blocks (edit mode)
        if (!isPauseMenu)
        {
            // Punch block
            if (button1.GetStateDown(trackedObj.inputSource))
            {
                rigid.AddForce(hand.transform.forward * jetstrength * Time.deltaTime);
            }
            
            // Kick block
            if (button2.GetStateDown(trackedObj.inputSource))
            {
                rigid.AddForce(-foot.transform.up* jetstrength * Time.deltaTime);
            }

            // change modes
            if (padTouched && padZones.up == padZone)
            {
                //handMode = limbModes.grab;
            }
        }
        // pause menu (laser pointer)
        if (isPauseMenu)
        {
            if (isPauseMenuPointer)
            {
                laserVisual.gameObject.SetActive(true);

                Ray raycast = new Ray(laserMuzzle.position, laserMuzzle.forward);
                anim.SetBool("pauseGrip", true);
                RaycastHit hit;
                bool rayHit;
                //if(songMgr.isDeleteMode) rayHit = Physics.Raycast(raycast, out hit, 50, laserMask, QueryTriggerInteraction.Collide);
                rayHit = Physics.Raycast(raycast, out hit, 50, laserMaskMenuOnly);
                Debug.DrawRay(laserMuzzle.position, laserMuzzle.forward, Color.red);
                if (rayHit)
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, hit.point);

                    Button but = hit.transform.GetComponent<Button>();
                    Toggle tog = hit.transform.GetComponent<Toggle>();
                    InputField inp = hit.transform.GetComponent<InputField>();
                    if (but != null)
                    {
                        EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            but.onClick.Invoke();
                        }
                    }
                    else if(tog != null)
                    {
                        EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            tog.isOn = !tog.isOn;
                        }
                    }
                    else if(inp != null)
                    { 
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        }
                    }
                    else if(hit.transform.name == "scrollbar-song")
                    {

                        if (button1.GetState(trackedObj.inputSource))
                        {
                            // song scrolling (dont want to translate 2d ui position so its hard coded)
                            Vector3[] corners = new Vector3[4];
                            hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                            float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[3]);
                            //songMgr.scrubSongTime(songMgr.music.clip.length * dist - songMgr.music.time);
                        }
                    }
                    else if(hit.transform.name == "Scrollbar Vertical")
                    {
                        // song scrolling (dont want to translate 2d ui position so its hard coded)
                        Vector3[] corners = new Vector3[4];
                        hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                        float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[2]);
                        hit.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = dist;
                    }

                    // delete mode
                    if (hit.transform.tag == "block")
                    {
                    }
                    else
                    {
                        if (laserVisColor != 1)
                        {
                            laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.green);
                            laserVisColor = 1;
                        }
                    }
                }
                else
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, laserMuzzle.position + laserMuzzle.forward * 50);
                    anim.SetBool("pauseGrip", false);
                    if (laserVisColor != 0)
                    {
                        laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.white);
                        laserVisColor = 0;
                    }
                }
            }
            else
            {
                laserVisual.gameObject.SetActive(false);
            }

            // pausemode trackpad
            if (padClickedDown && padZone == padZones.dead)
            {
            }
            if (padClickedDown && padZone == padZones.up)
            {
            }
            if(padTouched && padZone == padZones.up)
            {
            }
            else if (padTouched && padZone == padZones.down)
            {
            }

        }
        else
        {
            laserVisual.gameObject.SetActive(false);
        }
        
        // pause/unpause (toggles menu)
        if (menu.GetStateDown(trackedObj.inputSource))
        {
        }
    }

    public bool isbutton1Down()
    {
        return button1.GetState(trackedObj.inputSource);
    }
}
