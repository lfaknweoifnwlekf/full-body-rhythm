﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floatManager : MonoBehaviour
{
    public bool paused = true;

    [SerializeField]
    private GameObject menu;
    private Rigidbody rigid;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void pauseToggle()
    {
        menu.SetActive(!paused);
        rigid.isKinematic = paused;
    }
}
