# README #
This is a VR rhythm game designed for full-body tracking using SteamVR and the Unity Engine.
Players hit blocks with their hands, feet and hips to the rhythm of the music for points.

Game page: http://teh_bucket.itch.io/full-body-rhythm-vr
Source code: https://bitbucket.org/lfaknweoifnwlekf/full-body-rhythm/src/master/
See RhythmFBT/readme.txt for end-user documentation

### Unincluded Dependencies: ###
Unity Editor 2018.4.0f1 (free)
SteamVR for Unity plugin v2.2.0 (free, see unity store)

### License ###
CC0 Public Domain
https://creativecommons.org/share-your-work/public-domain/cc0/

### Credits ###
There is music included in the source files. The music is also licensed as above. The music was made by the following artists:
Joth https://soundcloud.com/user-215083110
Fupi https://opengameart.org/users/fupi