SEIZURE WARNING:
Some elements of the screen will blink or flash in time to the music. Don't play if you are prone to seizures.

RHYTHM GAME INFO:
Game is designed for SteamVR using Vive headset + 2 vive wand controllers + 3 additional vive trackers (hip + 2 feet). Other steamvr systems should still work, you may have to remap controls for this app using steam's system. 
Game involves punching/kicking/hip-bumping targets as they approach you. Targets can only be hit by the appropriate limbs, i.e. hands/feet/hip.
Game has a scoring system and a failure system. To play without failure, enable godmode.
Game has a built in level editor, see below.

NOTES ON CAUTION:
It is easy to hurt yourself, others, or property while playing this game. Please exercise caution.
You should have sufficient room to swing all of your limbs in every direction. Some levels may require an even larger playspace.

NOTES ON LICENSE:
This software is Public Domain (CC0). You can do anything with the source files, be it commercial, closed source, anything. I encourage any further development of the game.
Some included songs may have individual licenses. please see CREDITS.txt.


CONTROLS:
(gameplay has no controls, just movement. Menu/editor controls are as seen below)
global:
	both triggers- apply tracker offsets	
	menu - toggle pause menu
pausemenu only:
	trackpad click up: Restart song in play mode
	trackpad click center: Restart song in edit mode
	playmode + pausemenu only:
		trackpad click left/right: rotate playspace
	editmode + pausemenu only:
		button2: enter delete mode. button2 again to exit. point at block and hit button1 to delete.
			
		trackpad click left/right: scrub through song time
		trackpad click down: reset block binds for button1/button2
editmode only:
	(block-specific: see below)

SCORING:
blocks have score values and a goal-time to be hit. the closer to that time you hit them, the more points you score, up to 8x the original amount. Blocks will appear to glow more and blink faster closer to the goal-time. 
High scores are saved locally.

LEVEL EDITOR CONTROLS / BLOCKLIST:
Use controllers and trackers in real time to spawn blocks in time to the music corresponding to your motions.

Punch-Block: Swing your hand and press button1 (GrabPinch) on the corresponding hand.

Kick-Block: Swing your foot and press button2 (GrabGrip) on the hand corresponding to the foot.

Hip-Block: Swing your hips and click UP on either trackpad. (hover for visuals of these controls)

Hand Grind (always goes forward): Hold hand in position and click OUTER trackpad on corresponding controller. (Left-trackpad on left hand, Right-trackpad on right hand)

Foot Grind (always goes forward): Hold foot in position and click INNER-UP trackpad on corresponding controller. (Upper-Right-trackpad on left hand, Upper-left-trackpad on right hand)

Hip Grind: Hold hips in position and click DOWN on either trackpad.

Foot Tap (always goes down): Hold foot in position and click INNER-DOWN trackpad on corresponding controller. (Down-Left trackpad on left hand, Down-right trackpad on right hand)

Dodge Block: Swing hand and click CENTER trackpad on controller. Can hit any body part with negative points.

Note: You can bind a trackpad block to button1 or button2 by hovering over the block and then pressing button1/button2. Press button2 while in menu to reset binds back to hand/foot blocks.

LEVEL EDITOR SETTINGS:
Audio files must be .ogg (maybe mp3 works on your machine)
Place audio files in songs folder:
	Windows: "C:\Users\YOU\AppData\LocalLow\Teh_Bucket\Full_Body_Rhythm\songs\"
Levels get saved to the levels folder:
	Windows: "C:\Users\YOU\AppData\LocalLow\Teh_Bucket\Full_Body_Rhythm\levels\"

Level Name: will become filename (don't type extension)
Song File Name: song file to load from songs folder
Song Name: Displayed on Load. (use song name/artist, etc)
Author: this is you
Difficulty: describe difficulty, playspace requirements, etc
BPM: will do visual effects (todo)

SHARING LEVELS:
Assuming I haven't developed a level sharer yet, to send someone a level, just send them the level.json as well as the audio file. See above.
If the song is licensed for redistribution (any Creative Commons will do), with your permission, I'll add it to the base game.

TROUBLESHOOTING:
Like most fbt games, always turn on your controllers before you turn on your trackers. You may need to restart steamvr.
If you have problems with blocks not showing in edit mode, try scrubbing through, or resetting the level. Newly added blocks should reset to their appropriate places.



CREDITS:
MUSIC: 
Joth 
	https://soundcloud.com/user-215083110
	CC0 https://creativecommons.org/publicdomain/zero/1.0/
Fupi 
	https://opengameart.org/users/fupi 
	CC0 https://creativecommons.org/publicdomain/zero/1.0/
neocrey 
	http://neocrey.bandcamp.com
	CC-BY-SA 3.0 http://creativecommons.org/licenses/by-sa/3.0/
Kevin Macleod
	https://incompetech.com
	CC-BY http://creativecommons.org/licenses/by/4.0/
Mutkanto
	https://soundcloud.com/mutkanto
	https://creativecommons.org/publicdomain/zero/1.0/
Miknus
	Used with Permission
Stefon1A
	https://creativecommons.org/publicdomain/zero/1.0/


LICENSES:
CORE GAME LICENSE:
The core game is public domain and open source. The source is at https://bitbucket.org/lfaknweoifnwlekf/full-body-rhythm/src/master/
	CC0 https://creativecommons.org/publicdomain/zero/1.0/

ADDITIONAL MUSIC LICENSES:
Several songs are distributed alongside the game, but separately. Each file may have its own license. Authors are listed in credits. Each file should have the license, author, and more info in its metadata.

MORE INFO:
game page:
http://teh_bucket.itch.io/full-body-rhythm-vr
source code:
https://bitbucket.org/lfaknweoifnwlekf/full-body-rhythm/src/master/