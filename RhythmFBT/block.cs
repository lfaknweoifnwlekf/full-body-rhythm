﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class blockSave
{
    public int limbid = 0; // 0-4 for limbs (0 LH, 1 RH, 2 LF, 3 RF, 4 H) (for score i guess, or left/right specific hits)
    public float spawnTime = 0;
    public Vector3Serializable placePos; // place where it was spawned / will be hit
    public QuaternionSerializable placeRot;
    public Vector3Serializable moveDir;
    public float minForce;
    public Vector3Serializable requiredDirection;
    public int prefabID; // for save/load

    public blockSave(int limbid_, float spawnTime_, Vector3 placePos_, Quaternion placeRot_, Vector3 moveDir_, float minForce_, Vector3 requiredDirection_, int prefabID_)
    {
        limbid = limbid_;
        spawnTime = spawnTime_;
        placePos = placePos_;
        placeRot = placeRot_;
        moveDir = moveDir_;
        minForce = minForce_;
        requiredDirection = requiredDirection_;
        prefabID = prefabID_;
    }
}
public class block : MonoBehaviour
{
    public GameObject hitEffect;
    public GameObject explodeEffect;


    [Header("Generated attributes")]
    public int limbid = 0; // 0-4 for limbs (0 LH, 1 RH, 2 LF, 3 RF, 4 H) (for score i guess, or left/right specific hits)
    public float spawnTime = 0;
    public Vector3 placePos; // place where it was spawned / will be hit
    public Quaternion placeRot; 
    public Vector3 moveDir;
    public Vector3 requiredDirection;
    public float minForce = 10;
    public int prefabID; // for save/load, refers to array in songManager

    [Header("Prefab Settings")]
    public bool dieOnHit = true;
    public bool dullOnHit = false;
    public bool hurtOnHit = false;
    public bool requireDirectionMatch = false;
    public bool requireForce = true;
    public float minForcedefault = 2;
    [Tooltip("Angle between player's swing and block's orientation")] public float requiredDirectionAccuracy = 40F;
    public bool requireSubColliderContact = false;
    public Collider subCollider;
    public int points = 1;
    public int pointMultiplier = 1; // increases near placePos
    public float hp = 1;
    public float speed = 1; // meters/second
    public bool hasDefaultMoveDir = false;
    public Vector3 moveDirDefault;
    public bool hasDefaultPlaceRot = false;
    public Vector3 placeRotDefault;
    public bool hasDefaultYPos = false;
    public float defaultYPos = 0;
    public bool forwardizeOnSpawn = false;
    public float forwardizeAmount = 10;

    [Header("Visuals")]
    public bool randomizeRotation = false;
    public Renderer[] rend;
    public TrailRenderer trail;
    public Material advancedShader;
    public Material advancedShader2;
    public Material simpleShader;
    public Material colorBlindShader;
    public bool disableShader = false;
    private float matsetting1 = .25F;
    private float matsetting1bpm = .25F;
    private float matsetting2 = 1F;
    private float matsetting2bpm = 1F;
    public float currentbpm = 100;
    private float matsetting3 = 1;
    public float explodeMultiplier = 1;

    [Header("Other")]
    public bool isLive = false;
    public float moveMultiplier = 1; // reverses when placed

    public songManager songMgr;
    public AudioSource music;

    public Rigidbody rigid;
    private bool hasRigid = true;
    public Collider[] collid;
    public Animator anim;
   [HideInInspector] public bool hasAnim;
    private bool isDull = false;
    public bool isEditMode = false;
    
    // non-rigidbody death visual stuff
    private float dieTime;
    private Vector3 deathDir;
    private Vector3 deathDir2;
    private Vector3 defaultScale;
    private float deathSpeed;
    public bool isDebugBlock = false;
    public bool debugForceAwake = false;
    private bool wasDebugBlock = false;
    public bool isSleeping = false;
    private bool isSleepingStarted = false;

    private Transform playertarget;
    public float sleepUntil = 60F; // how many seconds ahead should it spawn

    private void OnCollisionEnter(Collision collision)
    {
        if (isLive && Time.timeScale > 0)
        {
            trackerPower playerLimb = collision.collider.GetComponentInParent<trackerPower>();
            bool subColPass = !requireSubColliderContact;
            if (requireSubColliderContact) foreach (ContactPoint contact in collision.contacts) if (contact.thisCollider.transform.name == subCollider.transform.name || contact.otherCollider.transform.name == subCollider.transform.name) subColPass = true;
            checkHit(collision.collider, playerLimb, collision.contacts[0].point, collision.contacts[0].normal, subColPass);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isLive &&  Time.timeScale > 0) {
            trackerPower playerLimb = other.transform.GetComponentInParent<trackerPower>();
            //Debug.Log(collision.collider.transform.name);

            checkHit(other, playerLimb, other.ClosestPointOnBounds(transform.position), transform.position - other.transform.position, true);
        }
    }

    public void checkHit(Collider other, trackerPower playerLimb, Vector3 point, Vector3 normal, bool subColMatch)
    {

        bool forcePass = (!requireForce || playerLimb.chargedAmount + playerLimb.swingPower > minForce);
        bool dirPass = (!requireDirectionMatch || Vector3.Angle(playerLimb.swingDir, requiredDirection) <= requiredDirectionAccuracy); // only counts if player swung in right direction
        if (!forcePass && !dirPass)
        {
            // pass both if swing is very strong but only slightly accurate
            if (playerLimb.chargedAmount + playerLimb.swingPower > minForce * 2 && Vector3.Angle(playerLimb.swingDir, requiredDirection) <= requiredDirectionAccuracy / 3)
            {
                forcePass = true;
                dirPass = true;
            }
        }
        bool subColPass = (!requireSubColliderContact || subColMatch);


        if (playerLimb != null && // only counts if hit by player
            isLive && // no edit mode or rehits
            forcePass && // only counts if player swung hard enough
            dirPass &&
            subColPass) //only counts if player hit the right collider
        {
            if (dieOnHit)
            {
                setAsHit(point, normal, playerLimb.swingDir * playerLimb.swingPower);
            }
        }
        else if (dullOnHit && isLive)
        {
            /* HIT DEBUG OUTPUT */
            if (isDebugBlock || songMgr.debughits)
            {
                List<string> dlog = new List<string>();
                dlog.Add("Dulled " + songMgr.blockPrefabs[prefabID].transform.name);
                if (playerLimb == null) dlog.Add("no Playerlimb in: " + other.transform.name);
                if (!forcePass) dlog.Add("Force insufficient: " + playerLimb.chargedAmount + playerLimb.swingPower + "/" + minForce);
                if (!dirPass) dlog.Add("swingDir off kilter: " + Vector3.Angle(playerLimb.swingDir, requiredDirection) + "/" + requireDirectionMatch);
                if (!subColPass) dlog.Add("subCol '" + subCollider.transform.name + " not hit.");
                Debug.Log(string.Join(" || ", dlog));
            }
            pointMultiplier = -4;
            setDull();
        }
    }

    public void setDull()
    {        
        isLive = false;
        isDull = true;
        foreach(Collider c in collid) c.enabled = false;
        dieTime = Time.timeSinceLevelLoad;
        shaderPointEffects(0);
        if (hasRigid) rigid.isKinematic = false;
        if (hasRigid) rigid.useGravity = true;
        if (hasAnim) anim.SetBool("isDull", true);

        else
        {
            deathDir = Vector3.Lerp(moveDir, Random.insideUnitSphere.normalized, Random.Range(0,.2F));
            deathDir2 = Vector3.down;
            deathSpeed = Random.Range(1F, 25F);
        }
        StartCoroutine(deathCountdown(false));
        songMgr.scoreIncrement(points, pointMultiplier, prefabID);
    }
    public void setAsHit(Vector3 hitPoint, Vector3 hitNorm, Vector3 swingDir)
    {
        isLive = false;
        moveMultiplier = 0;

        foreach(Collider c in collid) c.enabled = false;
        dieTime = Time.timeSinceLevelLoad;
        if (hasRigid)
        {
            rigid.useGravity = true;
            rigid.isKinematic = false;
            rigid.AddForce(swingDir * 1500 * explodeMultiplier);
        }
        if (hasAnim) anim.SetBool("isHit", true);
        else
        {
            if (requireForce)
            {
                deathDir = swingDir;
                deathDir2 = Vector3.down * 3;
                deathSpeed = 2000*swingDir.magnitude * explodeMultiplier;
            }
            else
            {

                deathDir = swingDir.magnitude * Random.insideUnitSphere;
                deathDir2 = Vector3.down * 2;
                deathSpeed = Random.Range(.5F, 2 + 6 * swingDir.magnitude);
            }
        }
        StartCoroutine(deathCountdown(true));
        if (songMgr.menu.showBlockParticles && hitEffect != null)
        {
            GameObject hitThing = Instantiate(hitEffect, hitPoint, Quaternion.LookRotation(hitNorm)) as GameObject;
            Renderer hitThingRend = hitThing.GetComponent<Renderer>();
            hitThingRend.material.SetColor("_EmissionColor", hitThingRend.material.GetColor("_EmissionColor") * pointMultiplier);
        }
        songMgr.scoreIncrement(points, pointMultiplier, prefabID);
    }

    IEnumerator deathCountdown(bool explode)
    {
        yield return new WaitForSeconds(currentbpm/30);
        if (explode && songMgr.menu.showBlockParticles && explodeEffect != null)
        {
            GameObject deaththing = Instantiate(explodeEffect, transform.position, transform.rotation) as GameObject;
        }
        gameObject.SetActive(false);
    }

    public void setPosTime(float time, bool rot=false)
    {
        if (time != 0)
        {
            transform.position = placePos - (moveDir * (spawnTime - time) * speed * moveMultiplier);
            if (rot) transform.rotation = placeRot;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        hasRigid = rigid != null;
        collid = GetComponentsInChildren<Collider>();
        anim = GetComponent<Animator>();
        hasAnim = anim != null;
        defaultScale = transform.localScale;
        matsetting1 = advancedShader.GetFloat("_fadespeed");
        matsetting2 = advancedShader.GetFloat("_blinkspeed");
        matsetting3 = advancedShader.GetFloat("_emitMultiplier");
        isEditMode = true;

        if (hasRigid) rigid.constraints = RigidbodyConstraints.None;
        if (hasRigid) rigid.isKinematic = true;
        foreach(Collider c in collid) c.enabled = false; // enable when close to player
        //speed *= 20;
        if(minForcedefault<minForce) minForce = minForcedefault;
        
        if (moveDir == Vector3.zero || spawnTime == 0)
        {
            gameObject.SetActive(false);
        }
        sleepUntil = 120 / speed;
    }

    public void StartPostSpawn(float musicTime = 0, float bpm = 0)
    {
        currentbpm = bpm;

        matsetting1bpm = matsetting1 * bpm / 60;
        matsetting2bpm = matsetting2 * bpm / 60;
        //Debug.Log("postspawn: " + transform.name + " musicTime:" + musicTime + "spawnTime:" + spawnTime + "sleepUntil" + sleepUntil + "startsleeping:"+(Mathf.Abs(musicTime - spawnTime) > sleepUntil));
        if (Mathf.Abs(musicTime - spawnTime) > sleepUntil && !debugForceAwake) StartCoroutine(sleepUntilClose());
        else
        {
            isSleeping = false;
            showHide(true);
            if (prefabID == 7)
            {
                StopCoroutine("showDownIndicator");
                StartCoroutine(showDownIndicator(Mathf.Max(Mathf.Abs(musicTime - spawnTime) - 4 / (currentbpm / 60), 0)));
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (isLive && !isSleeping && !isEditMode && Time.timeScale != 0)
        {
            // TODO: visual effects and scoring relative to placePos (for the sake of rhythm) (also bpm if i ever do that)
            float proximity = Mathf.Abs(music.time - spawnTime);
            // if(isDebugBlock)  Debug.Log(proximity + ", " + currentbpm / 60 / 8 + ", pm: "+pointMultiplier);
            if (proximity < currentbpm / 15)
            {
                foreach(Collider c in collid) c.enabled = true;
            }
            if (proximity < currentbpm / 60 / 16)
            {
                if (pointMultiplier != 4)
                {
                    if (isDebugBlock) Debug.Log("prox4");
                    shaderPointEffects(4);
                    pointMultiplier = 4;
                }
            }
            else if (proximity < currentbpm / 60 / 8)
            {
                if (pointMultiplier != 2)
                {
                    if (isDebugBlock) Debug.Log("prox2");
                    shaderPointEffects(2);
                    pointMultiplier = 2;
                }
            }
            else if (music.time > spawnTime + currentbpm / 60)
            {
                pointMultiplier = -8;
                if (isLive) setDull();
                else
                {
                    shaderPointEffects(0);
                }
            }
            else
            {
                if (pointMultiplier != 1)
                {
                    if (isDebugBlock) Debug.Log("prox1");

                    shaderPointEffects(.5F);
                    pointMultiplier = 1;                    
                }
            }
            if(proximity > sleepUntil)
            {
                if (!isSleepingStarted) StartCoroutine(sleepUntilClose());
            }
            // move away/towards center at fixed rate
            setPosTime(music.time); // why didnt i start with this
            //if (proximity < 5 && hasRigid) rigid.MovePosition(rigid.position + (moveDir * moveMultiplier * speed * Time.deltaTime));
            //else transform.position = rigid.position + (moveDir * moveMultiplier * speed * Time.deltaTime);
        }
        else if(!isLive && !isSleeping && !isEditMode && Time.timeScale != 0)
        {            
            float proxDeath = (Time.timeSinceLevelLoad - dieTime)/(currentbpm/30);
            if (isDebugBlock) Debug.Log("proxdeath: " + proxDeath);
            if (!hasRigid)
            {                
                transform.Translate(Vector3.Lerp(deathDir*deathSpeed, deathDir2*deathSpeed, proxDeath) * speed*2*Time.deltaTime, Space.World);
            }
            if (!hasAnim)
            {
                transform.localScale = Vector3.Lerp(defaultScale, Vector3.zero, proxDeath*2);
                if (isDull) rend[0].material.SetFloat("_dullness", proxDeath*2);
                if (isDull && rend.Length >1) rend[1].material.SetFloat("_dullness", proxDeath*2);
                if(!isDull) rend[0].material.SetFloat("_emitMultiplier", matsetting3 * (1 + proxDeath));
                if(!isDull && rend.Length > 1) rend[1].material.SetFloat("_emitMultiplier", matsetting3 * (1 + proxDeath));
            }
        }
        if (isEditMode && !isSleeping && Time.timeScale != 0)
        {
            float proximity = Mathf.Abs(music.time - spawnTime);
            if (proximity > sleepUntil / 4 || isSleepingStarted)
            {
                setPosTime(music.time);
            }
            else StartCoroutine(sleepUntilClose());
        }

        if(isDebugBlock && !wasDebugBlock)
        {
            wasDebugBlock = true;
            showHide(true);
        }
        else if(!isDebugBlock && wasDebugBlock)
        {
            wasDebugBlock = false;
            showHide(false);
        }
    }

    public void showHide(bool show)
    {
        if (show)
        {
            setPosTime(music.time);
            if (trail != null && songMgr.menu.showBlockTrails) trail.enabled = true;
            if (hasAnim && songMgr.menu.showBlockAnim) anim.enabled = true;
            foreach (Renderer r in rend)
            {
                r.enabled = true;
            }
            if (isDebugBlock) Debug.Log("show block");
            isSleeping = false;
        }
        else
        {
            isSleeping = true;
            if (hasAnim) anim.enabled = false;
            if (trail != null) trail.enabled = false;
            foreach (Collider c in collid) c.enabled = false;
            foreach (Renderer r in rend)
            {
                r.enabled = false;
            }
            if (isDebugBlock) Debug.Log("hide block");
        }
    }
    IEnumerator sleepUntilClose()
    {
        isSleepingStarted = true;
        //if (trail != null) trail.enabled = false;
        yield return new WaitForSeconds(1); // this fixed some weird bug but could cause problems
        showHide(false);

        isSleepingStarted = false;
        // sleep till close
        while (Time.timeScale !=0 && Mathf.Abs(music.time - spawnTime) > sleepUntil)
        {
            if (isDebugBlock) Debug.Log("Sleeping: " + transform.name + " musicTime:" + music.time + "spawnTime:" + spawnTime + "sleepUntil" + sleepUntil);
            yield return new WaitForSeconds(1);
        }
        showHide(true);
        if (prefabID == 7) StartCoroutine(showDownIndicator(Mathf.Max(sleepUntil - 4/(currentbpm/60), 0)));
    }
    public void startSleeping()
    {
        StartCoroutine(sleepUntilClose());
    }

    IEnumerator showDownIndicator(float delay)
    {
        yield return new WaitForSeconds(delay);
        if(songMgr.menu.downIndicatorCooldown) songMgr.downIndicator();
    }

    // convert saved block into live block at start of play
    public void resetBlock(bool editMode, float bpm, bool restartedSong = false)
    {
        isEditMode = editMode;
        StopAllCoroutines();
        float musicTime = 0;
        if (!restartedSong) musicTime = music.time;
        pointMultiplier = 1;
        // bpm visuals
        transform.localScale = defaultScale;
        if (trail != null) trail.enabled = false;

        // reset grind visuals
        if (!isLive && !hasRigid) {
            //transform.localScale = defaultScale;
            if (isDull) rend[0].material.SetFloat("_dullness", 0);
            if (isDull && rend.Length > 1) rend[1].material.SetFloat("_dullness", 0);
            rend[0].material.SetFloat("_emitMultiplier", matsetting3);
            if (rend.Length > 1) rend[1].material.SetFloat("_emitMultiplier", matsetting3);
        }

        // PLAY MODE
        if (!editMode)
        {
            gameObject.SetActive(true);
            isSleeping = false;
            isSleepingStarted = false;
            if (hasRigid) rigid.constraints = RigidbodyConstraints.None;
            if (hasRigid) rigid.isKinematic = true;
            if (hasRigid) foreach(Collider c in collid) c.isTrigger = false;
            // minimize scattered movedirs: mostly just in front 
            //moveDir = new Vector3(moveDir.x, moveDir.y / 2, moveDir.z);

            // place
            transform.rotation = placeRot;

            setPosTime(musicTime);

            if (randomizeRotation)
            {
                transform.rotation = Random.rotation;
            }
            
            //vars
            moveMultiplier = 1;
            isLive = true;
            isDull = false;
            if (hasRigid)
            {
                rigid.isKinematic = false;
                rigid.velocity = Vector3.zero;
                rigid.useGravity = false;
            }
            if(spawnTime < sleepUntil || restartedSong) foreach(Collider c in collid) c.enabled = true;
            
            if (hasAnim)
            {
                anim.SetBool("isDull", false);
                anim.SetBool("isHit", false);
            }
            if (moveDir == Vector3.zero)
            {
                gameObject.SetActive(false);
            }
            StartPostSpawn(musicTime,bpm);
        }
        // EDIT MODE
        else
        {
            gameObject.SetActive(true);

            //foreach(Collider c in collid) c.enabled = false;
            if (hasRigid) foreach (Collider c in collid) c.isTrigger = true;
            isLive = false;
            moveMultiplier = 1;
            transform.rotation = placeRot;

            if (hasRigid) rigid.isKinematic = true;
            if(hasRigid) rigid.useGravity = false;
            if ( moveDir == Vector3.zero)
            {
                gameObject.SetActive(false);
            }
            setPosTime(musicTime);
            StartPostSpawn(musicTime,bpm);
        }
        if (trail != null && songMgr.menu.showBlockTrails && Mathf.Abs(musicTime - spawnTime) < sleepUntil) trail.enabled = true;
        if (hurtOnHit) transform.LookAt(placePos); // dodge blocks
    }

    public void toggleTrail(bool val)
    {
       if (trail != null) trail.enabled = val;
    }
    public void toggleShader(bool val)
    {
        disableShader = !val;
        if (val)
        {
            foreach (Renderer r in rend)
            {
                if (r.materials.Length == 1) r.material = advancedShader;
                else
                {
                    if (advancedShader2 != null && r.materials.Length == 2)
                    {
                        Material[] newMats = { advancedShader, advancedShader2 };
                        r.materials = newMats;
                    }
                    else
                    {
                        Material[] newMats = r.materials;
                        for (int i = 0; i < newMats.Length - 1; i++) newMats[i] = advancedShader;
                        r.materials = newMats;
                    }
                }
            }
            shaderPointEffects(pointMultiplier);
        }
        else
        {
            foreach (Renderer r in rend)
            {

                if (r.materials.Length == 1) r.material = simpleShader;
                else
                {
                    Material[] newMats = r.materials;
                    for (int i = 0; i < newMats.Length - 1; i++) newMats[i] = advancedShader;
                    r.materials = newMats;
                }
            }
        }
    }
    public void toggleAnim(bool val)
    {
        if (hasAnim) anim.enabled = val;
    }

    public void toggleColorblind(bool isColorBlind, bool isShowBlockShaders)
    {
        if (isColorBlind && colorBlindShader != null)
        {
            foreach (Renderer r in rend) r.material = colorBlindShader;
        }
        else toggleShader(isShowBlockShaders);
    }

    private void shaderPointEffects(float x) {
        if (!disableShader)
        {
            foreach (Renderer r in rend)
            {
                foreach (Material m in r.materials)
                {                    
                    m.SetFloat("_fadespeed", matsetting1bpm * x);
                    m.SetFloat("_blinkspeed", matsetting2bpm * x);
                    m.SetFloat("_emitMultiplier", matsetting3 * x);
                }
            }
        }
    }

    public blockSave saveBlockData()
    {
        return new blockSave(limbid, spawnTime, placePos, placeRot, moveDir, minForce, requiredDirection, prefabID);
    }

    public void loadBlockData(blockSave s)
    {        
        limbid = s.limbid;
        spawnTime = s.spawnTime;
        placePos = s.placePos;
        placeRot = s.placeRot;
        moveDir = s.moveDir;        
        minForce = s.minForce;
        requiredDirection = s.requiredDirection;
        prefabID = s.prefabID;
    }

}
