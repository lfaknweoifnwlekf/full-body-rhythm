﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

// One script on each hand. Each refers to corresponding foot.
public class controllerRhythm : controllerGeneric
{
    public int handid = 0;
    public int footid = 2;
    
    [Header("Refernces")]
    public GameObject songBlocks;
    public songManager songMgr;
    public GameObject editorGauntlet;
    [SerializeField] private GameObject editorGauntletTriggerBinds;
    List<Transform> editorGauntletTriggerBindVisuals = new List<Transform>();
    [SerializeField] private GameObject editorGauntletGripBinds;
    List<Transform> editorGauntletGripBindVisuals = new List<Transform>();
    [SerializeField] private GameObject editorGauntletPadVisualsContainer;
    List<Animator> editorGauntletPadVisuals = new List<Animator>();

    // refers to songManager's blockPrefabs array
    private int handBlock = 0;
    private int footBlock = 1;
    private int hipBlock = 2;
    private int handGrind = 3;
    private int footGrind = 4;
    private int hipGrind = 5;
    private int dodgeBlock = 6;
    private int footPadBlock = 7;
    private int triggerBindBlock = 0;
    private int gripBindBlock = 1;
    // edited by songManager on mode change
    [HideInInspector] public bool isEditMode = true;
    [HideInInspector] public float timeSongStarted = 0;
    [HideInInspector] private bool canSpawnGrind = true;

    // Vector3 playerCenter;
    protected enum padZones
    {
        none = -1,
        trigger = 0,
        grip = 1,
        up = 2,
        left = 3,
        rightdown = 4,
        down = 5,
        dead = 6,
        rightup = 7,
        rightfull = 8,
    }
    private padZones padZone;
    private padZones oldPadZone;

    public float padDeadzone = .35F;

    private trackerPower handPower;
    private trackerPower footPower;
    private trackerPower hipPower;




    // Use this for initialization
    void Start()
        {

        genericStart();
                
        handPower = GetComponent<trackerPower>();
        footPower = foot.GetComponent<trackerPower>();
        hipPower = hip.GetComponent<trackerPower>();
        // block visuals (editor)
        foreach (Transform child in editorGauntletTriggerBinds.transform)
        {
            editorGauntletTriggerBindVisuals.Add(child);
        }
        foreach (Transform child in editorGauntletGripBinds.transform)
        {
            editorGauntletGripBindVisuals.Add(child);
        }
        foreach (Transform child in editorGauntletPadVisualsContainer.transform)
        {
            editorGauntletPadVisuals.Add(child.GetComponent<Animator>());
        }
        resetBinds();


        if (reverseTouchpad)
        {
            revPad = -1;
        }
    }

    
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        // Edit mode input
        if (isEditMode)
        {
            // Pad selection and editor visuals
            if (padTouch.GetState(trackedObj.inputSource))
            {
                padDir = padPos.GetAxis(trackedObj.inputSource);
                padClickedDown = padClick.GetStateDown(trackedObj.inputSource);
                padTouched = true;
                if (padTouchDistance < padDeadzone)
                {
                    padZone = padZones.dead;
                }
                else if (padAngle < -145 || padAngle > 135)
                {
                    padZone = padZones.down;
                }
                else if (padAngle < -90)
                {
                    if (isPauseMenu) padZone = padZones.rightfull;
                    else padZone = padZones.rightdown;
                }
                else if (padAngle < -35)
                {
                    if (isPauseMenu) padZone = padZones.rightfull; 
                    else padZone = padZones.rightup;
                    
                }
                else if (padAngle < 45)
                {
                    padZone = padZones.up;
                }
                else
                {
                    padZone = padZones.left;
                }

                if (oldPadZone != padZone)
                {
                    if (oldPadZone >= 0) editorGauntletPadVisuals[(int)oldPadZone].SetBool("isHighlighted", false);
                    if (padZone >= 0) editorGauntletPadVisuals[(int)padZone].SetBool("isHighlighted", true);
                    //Debug.Log("angle: " + padAngle + " dist: " + padTouchDistance + " other: " + padDir.x + ", " + padDir.y + "," + padZone);
                }

                oldPadZone = padZone;
            }
            else
            {
                if (padZone >= 0) editorGauntletPadVisuals[(int)padZone].SetBool("isHighlighted", false);
                padZone = padZones.none;
                oldPadZone = padZones.none;
                padTouched = false;
            }

            if (!isPauseMenu)
            {

                // Spawn blocks (edit mode)

                // Button1
                if (button1.GetStateDown(trackedObj.inputSource))
                {
                    if (padTouched)
                    {
                        setTriggerBlock((int)padZone);
                    }
                    else
                    {
                        if (triggerBindBlock == footBlock) spawnCube(footBlock, footPower.chargeRenderer.transform, footid, footPower.swingDir, footPower.swingPower);
                        else if (triggerBindBlock == handBlock) spawnCube(handBlock, transform, handid, handPower.swingDir, handPower.swingPower);
                    }
                }

                // Button2
                if (button2.GetStateDown(trackedObj.inputSource))
                {
                    if (padTouched)
                    {
                        setGripBlock((int)padZone);
                    }
                    else
                    {
                        if (gripBindBlock == footBlock) spawnCube(footBlock, footPower.chargeRenderer.transform, footid, footPower.swingDir, footPower.swingPower);
                        else if (gripBindBlock == handBlock) spawnCube(handBlock, transform, handid, handPower.swingDir, handPower.swingPower);
                    }
                }

                // Hip block
                if (padClickedDown && padZone == padZones.up || (!padTouched && triggerBindBlock == 2 && button1.GetStateDown(trackedObj.inputSource)) || (!padTouched && gripBindBlock == 2 && button2.GetStateDown(trackedObj.inputSource)))
                {
                    spawnCube(hipBlock, hipPower.chargeRenderer.transform, 4, hipPower.swingDir, hipPower.swingPower);
                }

                // Grind Hand
                if ((canSpawnGrind && padClick.GetState(trackedObj.inputSource) && padZone == padZones.left) || (canSpawnGrind && !padTouched && triggerBindBlock == 3 && button1.GetState(trackedObj.inputSource)) || (canSpawnGrind && !padTouched && gripBindBlock == 3 && button2.GetState(trackedObj.inputSource)))
                {
                    spawnCube(handGrind, transform, handid, handPower.swingDir, handPower.swingPower);
                    StartCoroutine(spawnDelay());
                }

                // Grind Foot
                if ((canSpawnGrind && padClick.GetState(trackedObj.inputSource) && padZone == padZones.rightdown) || (canSpawnGrind && !padTouched && triggerBindBlock == 4 && button1.GetState(trackedObj.inputSource)) || (canSpawnGrind && !padTouched && gripBindBlock == 4 && button2.GetState(trackedObj.inputSource)))
                {
                    spawnCube(footGrind, footPower.chargeRenderer.transform, footid, footPower.swingDir, footPower.swingPower);
                    StartCoroutine(spawnDelay());
                }

                // Grind Hip
                if ((canSpawnGrind && padClick.GetState(trackedObj.inputSource) && padZone == padZones.down) || (canSpawnGrind && !padTouched && triggerBindBlock == 5 && button1.GetState(trackedObj.inputSource)) || (canSpawnGrind && !padTouched && gripBindBlock == 5 && button2.GetState(trackedObj.inputSource)))
                {
                    spawnCube(hipGrind, hipPower.chargeRenderer.transform, 4, hipPower.swingDir, hipPower.swingPower);
                    StartCoroutine(spawnDelay());
                }

                // Dodge
                if (padClickedDown && padZone == padZones.dead || (!padTouched && triggerBindBlock == 6 && button1.GetStateDown(trackedObj.inputSource)) || (!padTouched && gripBindBlock == 6 && button2.GetStateDown(trackedObj.inputSource)))
                {
                    spawnCube(dodgeBlock, transform, 5, handPower.swingDir, handPower.swingPower);
                }

                // Foot-Pad (ddr)
                if (padClickedDown && padZone == padZones.rightup || (!padTouched && triggerBindBlock == 7 && button1.GetStateDown(trackedObj.inputSource)) || (!padTouched && gripBindBlock == 7 && button2.GetStateDown(trackedObj.inputSource)))
                {
                    spawnCube(footPadBlock, footPower.chargeRenderer.transform, footid, Vector3.down, 1); // foot pad has fixed place settings
                }
            }
        }
        // pause menu (laser pointer)
        if (isPauseMenu)
        {
            bool ignoreButton2Input = false; // disable grip bind this loop if it's used for a button
            if (isPauseMenuPointer)
            {
                laserVisual.gameObject.SetActive(true);

                Ray raycast = new Ray(laserMuzzle.position, laserMuzzle.forward);
                anim.SetBool("isPointing", true);
                RaycastHit hit;
                bool rayHit;
                if(songMgr.isDeleteMode) rayHit = Physics.Raycast(raycast, out hit, 50, laserMask, QueryTriggerInteraction.Collide);
                else rayHit = Physics.Raycast(raycast, out hit, 50, laserMaskMenuOnly);
                Debug.DrawRay(laserMuzzle.position, laserMuzzle.forward, Color.red);
                if (rayHit)
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, hit.point);

                    Button but = hit.transform.GetComponent<Button>();
                    buttonHelper buthelp = hit.transform.GetComponent<buttonHelper>();
                    Toggle tog = hit.transform.GetComponent<Toggle>();
                    InputField inp = hit.transform.GetComponent<InputField>();
                    if (but != null)
                    {
                        if(!fbtMgr.isTyping) EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            fbtMgr.isTyping = false;
                            but.onClick.Invoke();
                        }
                    }
                    else if(tog != null)
                    {
                        if (!fbtMgr.isTyping) EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            fbtMgr.isTyping = false;
                            tog.isOn = !tog.isOn;
                        }
                    }
                    else if(inp != null)
                    { 
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            fbtMgr.isTyping = true;
                            EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        }
                    }
                    else if(hit.transform.name == "scrollbar-song")
                    {

                        if (button1.GetState(trackedObj.inputSource))
                        {
                            // song scrolling (dont want to translate 2d ui position so its hard coded)
                            Vector3[] corners = new Vector3[4];
                            hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                            float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[3]);
                            songMgr.scrubSongTime(songMgr.music.clip.length * dist - songMgr.music.time);
                        }
                    }
                    else if(hit.transform.name == "Scrollbar Vertical")
                    {
                        if (button1.GetState(trackedObj.inputSource))
                        {
                            // scrolling (dont want to translate 2d ui position so its hard coded)
                            Vector3[] corners = new Vector3[4];
                            hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                            float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[2]);
                            hit.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = dist;
                        }
                    }
                    if (buthelp != null)
                    {
                        if (button1.GetState(trackedObj.inputSource)) setTriggerBlock(buthelp.intVal);
                        if (button2.GetState(trackedObj.inputSource))
                        {
                            ignoreButton2Input = true;
                            setGripBlock(buthelp.intVal);
                        }
                    }

                    // delete mode
                    if (songMgr.isDeleteMode && hit.transform.tag == "block")
                    {
                        if (laserVisColor != 2)
                        {
                            laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.red);
                            laserVisColor = 2;
                        }
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            block b = hit.transform.GetComponent<block>();
                            songMgr.blocks.Remove(b);
                            GameObject.Destroy(b.gameObject);
                        }
                    }
                    else
                    {
                        if (laserVisColor != 1)
                        {
                            laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.green);
                            laserVisColor = 1;
                        }
                    }
                }
                else
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, laserMuzzle.position + laserMuzzle.forward * 50);
                    anim.SetBool("isPointing", false);
                    if (laserVisColor != 0)
                    {
                        laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.white);
                        laserVisColor = 0;
                    }
                }
            }
            else
            {
                laserVisual.gameObject.SetActive(false);
            }

            // pausemode trackpad
            if (padClickedDown && padZone == padZones.dead)
            {
                // disabled since its more of an annoyance as misclick than helpful
                //songMgr.startLevelEdit();
            }
            if (padClickedDown && padZone == padZones.up)
            {
                // disabled since its more of an annoyance as misclick than helpful
                //songMgr.startLevel();
            }
            if(padTouched && padZone == padZones.up)
            {
                songMgr.scrollAll(.015F);
            }
            else if (padTouched && padZone == padZones.down)
            {
                songMgr.scrollAll(-.01F);
            }
            // trackpad rotate playspace (play only)
            if (!isEditMode)
            {
                if (padClick.GetState(trackedObj.inputSource) && (padZone == padZones.rightdown || padZone == padZones.rightup))
                {
                    fbtMgr.transform.RotateAround(fbtMgr.head.transform.position, Vector3.up, -1);
                }
                else if (padClick.GetState(trackedObj.inputSource) && padZone == padZones.left)
                {
                    fbtMgr.transform.RotateAround(fbtMgr.head.transform.position, Vector3.up, 1);
                }
                if (padClick.GetStateDown(trackedObj.inputSource) && padZone == padZones.down)
                {
                    // open 
                }
            }

            // end delete mode
            if (songMgr.isDeleteMode && button2.GetStateDown(trackedObj.inputSource))
            {
                songMgr.SetMode(songManager.gameMode.edit,false,false );
                ignoreButton2Input = true;
            }
            //  (edit only)
            if (isEditMode)
            {
                if (!ignoreButton2Input && button2.GetStateDown(trackedObj.inputSource))
                {
                    songMgr.startDeleteMode();
                }
                if (padClick.GetState(trackedObj.inputSource) && ((!reverseTouchpad && padZone == padZones.rightfull) || (reverseTouchpad && padZone == padZones.left)))
                {
                    songMgr.scrubSongTime(5 * Time.unscaledDeltaTime);
                }
                else if (padClick.GetState(trackedObj.inputSource) && ((!reverseTouchpad && padZone == padZones.left) || (reverseTouchpad && padZone == padZones.rightfull)))
                {
                    songMgr.scrubSongTime(-5 * Time.unscaledDeltaTime);
                }
                if(padClick.GetStateDown(trackedObj.inputSource) && padZone == padZones.down){
                    resetBinds();                    
                }
            }

        }
        else
        {
            laserVisual.gameObject.SetActive(false);
        }
        
        // pause/unpause (toggles menu)
        if (menu.GetStateDown(trackedObj.inputSource))
        {
            songMgr.pauseToggle();
        }
    }


    public void changeMode(bool isNowEditMode, bool isNowPaused)
    {
        isEditMode = isNowEditMode;
        isPauseMenu = isNowPaused;
        isPauseMenuPointer = isPauseMenu;
        timeSongStarted = Time.unscaledTime;
        anim.SetBool("isPaused", isPauseMenu);
        anim.SetBool("isEditMode", isEditMode);
    }
    public void resetBinds()
    {
        setTriggerBlock(0);
        setGripBlock(1);
    }

    public void setTriggerBlock(int n)
    {
        editorGauntletTriggerBindVisuals[triggerBindBlock].gameObject.SetActive(false);
        triggerBindBlock = n;
        editorGauntletTriggerBindVisuals[triggerBindBlock].gameObject.SetActive(true);
    }
    public void setGripBlock(int n)
    {
        editorGauntletGripBindVisuals[gripBindBlock].gameObject.SetActive(false);
        gripBindBlock = n;
        editorGauntletGripBindVisuals[gripBindBlock].gameObject.SetActive(true);
    }

    IEnumerator spawnDelay()
    {
        canSpawnGrind = false;
        yield return new WaitForSeconds(.05F);
        canSpawnGrind = true;
    }

    void spawnCube(int prefabID, Transform place, int id, Vector3 swingDir, float swingPower)
    {
        float spawnTime = songMgr.music.time;
        if (swingDir != Vector3.zero || (!songMgr.blockPrefabs[prefabID].GetComponent<block>().requireForce && prefabID != 6))
        {
            GameObject n = Instantiate(songMgr.blockPrefabs[prefabID], place.transform.position, place.transform.rotation) as GameObject;
            n.transform.parent = songBlocks.transform;
            block ns = n.GetComponent<block>();
            ns.limbid = id;
            ns.prefabID = prefabID;
            ns.spawnTime = spawnTime;
            ns.songMgr = songMgr;
            ns.music = songMgr.music;
            ns.placePos = place.position;

            if (ns.hasDefaultYPos)
            {
                ns.placePos = new Vector3(ns.placePos.x, ns.defaultYPos, ns.placePos.z);
            }
            if (swingDir.magnitude != 0) ns.placeRot = Quaternion.LookRotation(-swingDir);
            else ns.placeRot = Quaternion.Euler(Vector3.zero);
            ns.moveDir = Vector3.Normalize(-swingDir);
            ns.moveMultiplier = -1;
            if (ns.forwardizeOnSpawn)
            {
                ns.moveDir = Vector3.Normalize(new Vector3(-swingDir.x / ns.forwardizeAmount, -swingDir.y / ns.forwardizeAmount, -1)); // straight forward mostly
            }

            // Use defaults instead if applicable
            if (ns.hasDefaultMoveDir)
            {
                ns.moveDir = ns.moveDirDefault;
            }
            if (ns.hasDefaultPlaceRot)
            {
                ns.placeRot = Quaternion.Euler(ns.placeRotDefault);
                n.transform.rotation = Quaternion.Euler(ns.placeRotDefault);
            }

            //ns.moveDir.y = ns.moveDir.y / 3; // minimize up/down
            // force / alignment
            ns.minForce = swingPower * .5F;
            ns.requiredDirection = swingDir;
            n.transform.LookAt(transform.position - swingDir);
            if(!songMgr.menu.showBlockAnim) ns.toggleAnim(false);
            if (!songMgr.menu.showBlockTrails) ns.toggleTrail(false);
            if (songMgr.menu.showBlockColorblind) ns.toggleColorblind(true, false);
            else if (!songMgr.menu.showBlockShaders) ns.toggleShader(false);
            ns.isEditMode = true;
            ns.music = songMgr.music;

            songMgr.blocks.Add(ns);
            ns.StartPostSpawn(songMgr.music.time);

            // editor visuals
            editorGauntletPadVisuals[prefabID].SetTrigger("goGreen");
        }
        else
        { // failed to spawn block
            GameObject n = Instantiate(songMgr.errorSound, place.transform.position, place.transform.rotation) as GameObject;
            editorGauntletPadVisuals[prefabID].SetTrigger("goRed");
            // Debug.Log("Didn't spawn because: " + swingDir.magnitude + " or " + !songMgr.blockPrefabs[prefabID].GetComponent<block>().requireForce);
        }
    }
}
