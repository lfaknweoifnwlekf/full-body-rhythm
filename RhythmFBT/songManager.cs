﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
//using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;
using UnityEngine.Networking;

public class songManager : MonoBehaviour
{    
    [HideInInspector] public bool doSave = false;
    [Header("LOAD------")]
    public bool doLoad = false;
    [Header("LOADsng------")]
    public bool doLoadsng = false;
    [Header("debugHits------")]
    public bool debughits = true;
    [Header("bbbs test")]
    public bool tmpTest = false;
    public bool tmpTest2 = false;
    public float tmp2 = 2;

    [Header("Save Info")]
    public string levelName;
    public string songName;
    public string songFileName;
    public string stageFileName;
    public string author;
    public string difficulty;
    public float bpm = 100;

    [Header("Save Data")]
    [HideInInspector] public List<block> blocks;

    [Header("Scene References")]
    public AudioSource music;
    public AudioSource menumusic;

    public fullBodyAssign fbtMgr;
    public Animator sceneAnim;
    public GameObject songBlocks;
    public GameObject menuUI;
    public GameObject errorSound;


    [System.Serializable]
    public class MenuStuff
    {
        public Animator anim;
        [Header("Load")]
        public InputField toLoadLevelName;
        public Transform saveList;
        public GameObject saveLoadButtonTemplate;
        public Text loadDebugOut;
        public int saveButtonHeight = 86;
        public ScrollRect scrLoad;
        [Header("Save")]
        public Transform songList;
        public Text saveDebugOut;
        public GameObject songLoadButtonTemplate;
        public int songButtonHeight = 86;
        public InputField levelName;
        public InputField songName;
        public InputField songFileName;
        public InputField author;
        public InputField difficulty;
        public InputField bpm;
        public ScrollRect scrSong;
        [Header("Mode displays")]
        public Text modeDisplay;
        [Header("Other")]
        public GameObject desktopCamera;
        public List<float> bpmCount;
        public Text[] scoreViews;
        public Text[] songNameViews;
        public Text fpsView;
        public bool showFPS = true;
        public float fps = 0;
        public ScrollRect[] scrolls;
        public GameObject downIndicator;
        public Animator downIndicatoranim;
        public bool downIndicatorCooldown = true;
        public GameObject loadingScreen;
        public Renderer rotorscape;
        public LayerMask maskNormal;
        public LayerMask maskLoading;
        public GameObject tutorial;
        [Header("Editmode")]
        public GameObject editMenu;
        public GameObject saveMenu;
        public Scrollbar scrollSong;
        [Header("Score")]
        public Transform scoreGrid;
        public Text scoreTotal;
        public GameObject failed;
        public GameObject succeeded;
        public GameObject highscoreTemplate;
        public InputField scoreName;
        [HideInInspector] public bool savedHighScore = false;
        [HideInInspector] public List<List<int>> scoreBlockCount = new List<List<int>>(); // 0-7: blocks by prefabID. nested 0-5: score multipliers 0:x8, 1:x4, 2:x2, 3:x1, 4:x0, 5:x-1
        [HideInInspector] public List<List<Text>> scoreBlockTextOut = new List<List<Text>>(); // 0-7: blocks by prefabID. nested 0-5: score multipliers 0:x8, 1:x4, 2:x2, 3:x1, 4:x0, 5:x-1
        public Dictionary<int, int> scoreMultIndexes = new Dictionary<int, int>();
        public Dictionary<int, int> scoreMultIndexesReverse = new Dictionary<int, int>();
        [Header("Settings")]
        public bool showBlockTrails = true;
        public bool showBlockAnim = true;
        public bool showBlockShaders = true;
        public bool showBlockParticles = true;
        public bool showBlockColorblind = false;
        public bool isGodMode = true;
        public bool audioVisualizationEnabled = false;
        [Header("EditSettings")]
        public bool onlyFuture1 = true;
        public bool onlyFuture2 = true;
        public bool onlyFuture3 = true;
    }
    [Header("Menu")]
    public MenuStuff menu;

    [Header("Other stuff/UI")]
    public bool isStarted = false;
    public int pointFailureThreshold = -700;
    public int pointMaxFalloffStart = 800;
    public int pointMaxFalloffEnd = 1400;
    public enum gameMode
    {
        mainmenu = 0,
        edit = 1,
        play = 2,
    }
    public int points = 0;
    public int pointFailureStatus = 200; //same as points but maxes out, making failure more ever-threatening. starts out at 300 for easy start
    public gameMode mode = 0;
    public bool paused = true;
    public bool audioLoaded = false;
    public bool isDeleteMode = false;
    private string startupPath;

    [Header("Prefab List - Don't shuffle (affects save/loads)")]
    public GameObject[] blockPrefabs; // Don't shuffle: referenced by controller's edit mode and block.cs's save/load
    public Texture2D[] blockPrefabIcons;

    public void SetMode(gameMode setMode, bool resetBlocks = false, bool pause = false)
    {
        // perform reset if necessary (and if player failed level)
        if ((mode == gameMode.mainmenu && setMode != mode) || (paused && setMode == gameMode.play && !menu.isGodMode && pointFailureStatus < pointFailureThreshold))
        {
            resetBlocks = true;
        }
        // Change mode
        bool modeChanged = mode != setMode;
        bool pauseChanged = pause != paused;
        //Debug.Log("SetMode: " + setMode + ", " + resetBlocks + ", " + pause + ". pausechanged: " + pauseChanged + ", modechanged: " + modeChanged);
        mode = setMode;
        if (isDeleteMode) {
            endDeleteMode();
        }
        // Reset Blocks (start song)
        if (resetBlocks)
        {
            music.Stop();
            scoreReset();
            music.time = 0;
            foreach (Text t in menu.scoreViews) t.text = "0";
            //music.gameObject.SetActive(false);
            foreach (block n in blocks)
            {                
                n.gameObject.SetActive(true);
                n.resetBlock((mode == gameMode.edit), bpm, true);                
            }
            //music.gameObject.SetActive(true);
            blackoutCamera(false);
            music.Play();
            isStarted = true;
            menu.failed.SetActive(false);
            menu.succeeded.SetActive(false);
            menu.savedHighScore = false;            
        }
        // MODE CHANGE
        if (setMode == gameMode.edit && modeChanged)
        {
            menu.modeDisplay.text = "Edit Mode";
            foreach (block n in blocks)
            {
                n.isEditMode = true;
                n.gameObject.SetActive(true);
                n.resetBlock(true, bpm); // otherwise they get stuck
            }
            //menu.editMenu.SetActive(true);
            //menu.saveMenu.SetActive(true);
            scoreReset();
            foreach (Text t in menu.scoreViews) t.text = "--";
            RenderSettings.skybox.SetFloat("_AtmosphereThickness",2.5F);
        }
        else if (setMode == gameMode.play && modeChanged)
        {
            menu.modeDisplay.text = "Play Mode";
            foreach (block n in blocks)
            {
                n.isEditMode = false;
            }
            //menu.editMenu.SetActive(false);
            //menu.saveMenu.SetActive(false);
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", 5F);
        }
        // Pause/unpause, Toggle Menu
        if (pause)
        {
            menuUI.SetActive(true);
            menu.anim.SetBool("isEditMode", setMode == gameMode.edit);
            menu.failed.SetActive(false);
            menu.succeeded.SetActive(false);
            scoreUpdateInMenu();
            if (pauseChanged || resetBlocks)
            {
                Time.timeScale = 0;
                music.Pause();
                if (music.clip != null) menu.scrollSong.value = music.time / music.clip.length;
                menu.saveDebugOut.gameObject.SetActive(false);
            }
        }
        else if(!pause)
        {
            if (pauseChanged || resetBlocks)
            {
                Time.timeScale = 1;
                menuUI.SetActive(false);
                music.UnPause();
                menumusic.Stop();
            }            
        }
        menu.tutorial.SetActive(false);

        if(modeChanged || pauseChanged || resetBlocks) applyModeChangeExternally((mode == gameMode.edit), pause);
        paused = pause;
    }
    /* MENU STUFF */
    public void pauseToggle()
    {
        SetMode(mode, false, !paused);
    }
    public void restartLevel()
    {
        SetMode(mode, true, false);
    }
    public void toggleEditMode()
    {
        if (gameMode.edit == mode)
        {
            SetMode(gameMode.play, false, paused);
        }
        else
        {
            SetMode(gameMode.edit, false, paused);
        }
    }
    public void startLevel()
    {
        SetMode(gameMode.play, true, false);
    }
    public void startLevelEdit()
    {
        SetMode(gameMode.edit, true, false);
    }
    public void openSaveFileLocationInFileBrowser(string folder)
    {
        System.Diagnostics.Process.Start(Application.persistentDataPath + "//" + folder + "//");
    }
    public void LoadLevelByMenu()
    {
        StartCoroutine(LoadLevelBlackoutTimer(menu.toLoadLevelName.text));
    }
    public void LoadLevelByButton(string levelFullPath)
    {
        StartCoroutine( LoadLevelBlackoutTimer(levelFullPath, true));
    }
    IEnumerator LoadLevelBlackoutTimer(string path, bool fullpath = false)
    {
        blackoutCamera(true);
        //yield return new WaitForEndOfFrame();
        yield return new WaitForSecondsRealtime(.1F);
        LoadLevel(path, fullpath);
    }
    public void LoadSongOnly(string songFullPath, string songCosmetic, string songFile)
    {
        deleteAllBlocks(true);
        levelName = songCosmetic;
        menu.levelName.text = levelName;
        songName = songCosmetic;
        menu.songName.text = songName;
        foreach (Text snv in menu.songNameViews) snv.text = songName;
        songFileName = songFile;
        menu.songFileName.text = songFileName;
        author = "Anonymous";
        menu.author.text = author;
        bpm = 100;
        difficulty = "?";
        menu.difficulty.text = difficulty;
        StartCoroutine(LoadAudioFile(songFullPath));
    }
    public void setLevelName(string str)
    {
        levelName = str;
    }
    public void setSongName(string str)
    {
        songName = str;
    }
    public void setSongFileName(string str)
    {
        songFileName = str;
    }
    public void setAuthor(string str)
    {
        author = str;
    }
    public void setBPMFromText(string str)
    {
        bpm = float.Parse(str);
    }
    public void setDifficultyText(string str)
    {
        difficulty = str;
    }
    public void scrollUp(ScrollRect scr)
    {
        scr.verticalNormalizedPosition += .3F;
    }
    public void scrollDown(ScrollRect scr)
    {
        scr.verticalNormalizedPosition -= .3F;
    }
    public void scrollAll(float scroll)
    {
        foreach (ScrollRect scr in menu.scrolls) scr.verticalNormalizedPosition += scroll;
    }
    public void downIndicator()
    {
        if(menu.downIndicatorCooldown) StartCoroutine(downIndicatorAnimation());
    }
    IEnumerator downIndicatorAnimation()
    {
        if (menu.downIndicatorCooldown)
        {
            menu.downIndicatorCooldown = false;
            menu.downIndicator.SetActive(true);
            yield return new WaitForSeconds(4 / (bpm / 60) - 1);
            menu.downIndicatoranim.SetBool("deleted", true);
            yield return new WaitForSeconds(1);
            menu.downIndicatoranim.SetBool("deleted", false);
            menu.downIndicator.SetActive(false);
            yield return new WaitForSeconds(12 / (bpm / 60));
            menu.downIndicatorCooldown = true;
        }
        else yield return new WaitForSeconds(.1F);
    }
    public void toggleBlockTrails(bool val)
    {
        menu.showBlockTrails = val;
        foreach(block n in blocks)
        {
            if (n.trail != null) n.trail.enabled = menu.showBlockTrails;
        }
    }
    public void toggleBlockShaders(bool val)
    {
        menu.showBlockShaders = val;
        foreach (block n in blocks)
        {
            if(!menu.showBlockColorblind) n.toggleShader(val);
            /*
            n.disableShader = !menu.showBlockShaders;
            if (menu.showBlockShaders)
            {
                foreach (Renderer r in n.rend)
                {
                    if (r.materials.Length == 1) r.material = n.advancedShader;
                    else
                    {
                        if(n.advancedShader2 != null && r.materials.Length == 2)
                        {
                            Material[] newMats = { n.advancedShader, n.advancedShader2 };
                            r.materials = newMats;
                        }
                        else
                        {
                            Material[] newMats = r.materials;
                            for (int i = 0; i < newMats.Length - 1; i++) newMats[i] = n.advancedShader;
                            r.materials = newMats;
                        }
                    }
                }
            }
            else
            {
                foreach (Renderer r in n.rend)
                {

                    if (r.materials.Length == 1) r.material = n.simpleShader;
                    else
                    {
                        Material[] newMats = r.materials;
                        for (int i = 0; i < newMats.Length - 1; i++) newMats[i] = n.advancedShader;
                        r.materials = newMats;
                    }
                }
            }
            */
        }
    }
    public void toggleBlockColorblind(bool val)
    {
        menu.showBlockColorblind = val;
        foreach (block n in blocks)
        {
            n.toggleColorblind(val,menu.showBlockShaders);
        }
    }
    public void toggleBlockAnim(bool val)
    {
        menu.showBlockAnim = val;
        foreach(block n in blocks)
        {
            if (n.hasAnim) n.anim.enabled = menu.showBlockAnim;
        }
    }
    public void toggleBlockParticles(bool val)
    {
        menu.showBlockParticles = val;
    }
    public void toggleAudioVisualization(bool val)
    {
        menu.audioVisualizationEnabled = val;
    }
    public void toggleFutureOnly1(bool val)
    {
        menu.onlyFuture1 = val;
    }
    public void toggleFutureOnly2(bool val)
    {
        menu.onlyFuture2 = val;
    }
    public void toggleFutureOnly3(bool val)
    {
        menu.onlyFuture3 = val;
    }

    public void deleteAllBlocks(bool forceDeleteAll = true)
    {
        foreach (block b in blocks.ToList<block>())
        {
            if (forceDeleteAll || !menu.onlyFuture2 || b.spawnTime > music.time)
            {
                blocks.Remove(b);
                Destroy(b.gameObject);
            }
        }
    }
    public void deleteAllBlocksOfType(int prefabIndex)
    {        
        foreach(block b in blocks.ToList<block>())
        {
            if(b.prefabID == prefabIndex && (!menu.onlyFuture1 || b.spawnTime > music.time))
            {
                blocks.Remove(b);
                Destroy(b.gameObject);
            }
        }
    }
    public void scrubSongTime(float amount = 1)
    {
        float goTime = music.time + amount;
        if (goTime < 0) goTime = 0;
        else if (goTime > music.clip.length) goTime = music.time;
        foreach(block b in blocks)
        {
            if (Mathf.Abs(goTime - b.spawnTime) < b.sleepUntil / 4)
            {
                b.StopCoroutine("sleepUntilClose");
                if (b.isSleeping) b.showHide(true);
                b.moveMultiplier = 1;
                b.setPosTime(goTime, true);
            }
            else if (!b.isSleeping)
            {
                b.showHide(false);
                b.startSleeping();
            }
        }
        music.time = goTime;
        menu.scrollSong.value = music.time / music.clip.length;
        if (isDeleteMode) foreach (block b in blocks) b.collid[0].enabled = Mathf.Abs(goTime - b.spawnTime) < b.sleepUntil / 4; // reset delete mode colliders
        //Debug.Log("Scrubbed time +" + amount.ToString() + ", gotime:" + goTime.ToString() + ", time:"+music.time.ToString());
    }
    // hide menu but dont unpause
    public void hideMenu()
    {
        menuUI.SetActive(false);
    }
    public void forwardizeBlocks()
    {
        foreach(block b in blocks)
        {
            if (!b.hasDefaultMoveDir && (!menu.onlyFuture3 || b.spawnTime > music.time))
            {
                b.moveDir = new Vector3(b.moveDir.x, b.moveDir.y, -Mathf.Abs(b.moveDir.z));
                b.setPosTime(music.time);
            }
        }
    }
    public void startDeleteMode()
    {
        foreach (block b in blocks)
        {
            if (Mathf.Abs(music.time - b.spawnTime) < b.sleepUntil / 4)
            {
                b.collid[0].enabled = true;
            }
        }
        foreach (controllerGeneric hand in fbtMgr.hands) hand.GetComponent<controllerRhythm>().anim.SetBool("isDeleteMode", true);

        Debug.Log("started delete mode");
        isDeleteMode = true;
        hideMenu();
    }
    public void endDeleteMode()
    {
        foreach (block b in blocks) b.collid[0].enabled = false;
        foreach (controllerGeneric hand in fbtMgr.hands) hand.GetComponent<controllerRhythm>().anim.SetBool("isDeleteMode", false);
        Debug.Log("ended delete mode");
        isDeleteMode = false;        
    }
    IEnumerator showFPSUpdate()
    {
        while (menu.showFPS)
        {
            menu.fpsView.text = "fps: " + menu.fps;
            yield return new WaitForSecondsRealtime(.3F);
        }
    }
    public void toggleDesktopCamera(bool val)
    {
        menu.desktopCamera.SetActive(!menu.desktopCamera.activeSelf);
    }
    public void scoreIncrement(int n, int multiplier, int prefabID)
    {
        if (!paused)
        {
            int amount = n * multiplier;
            points += amount;
            // add points to get farther from failure, if already very far from failure, points provide less safety
            if (amount > 0 && pointFailureStatus + amount > pointMaxFalloffStart)
            {
                float falloffLength = pointMaxFalloffEnd - pointMaxFalloffStart;
                float depthIntoFalloff = pointFailureStatus + amount - pointMaxFalloffStart;
                pointFailureStatus += Mathf.Max(Mathf.FloorToInt((float)amount * ((falloffLength - depthIntoFalloff) / falloffLength)), 0);
            }
            else pointFailureStatus += amount;
            foreach (Text t in menu.scoreViews) t.text = points.ToString();

            // count specific block count/multiplier for score display
            int multindex = menu.scoreMultIndexes[multiplier];

            menu.scoreBlockCount[multindex][prefabID] += 1;
            menu.scoreBlockCount[5][prefabID] += n * multiplier; // increment total for this block

            // display failure level and fail (redo scoring)
            // RenderSettings.skybox.SetFloat("_Exposure", (float)Mathf.Min(points - pointFailureThreshold / Mathf.Abs(pointFailureThreshold * 2) * 5, 0));
            sceneAnim.SetFloat("failThreshold", ((float)pointFailureStatus - pointFailureThreshold) / ((float)pointMaxFalloffEnd - pointFailureThreshold));
            if (!menu.isGodMode && gameMode.play == mode && pointFailureStatus < pointFailureThreshold)
            {
                SetMode(gameMode.play, false, true);
                menu.failed.SetActive(true);
            }
        }
    }
    public void scoreUpdateInMenu()
    {
        menu.scoreTotal.text = points.ToString();

        int textallid = 0;
        for (int multid = 0; multid < menu.scoreMultIndexes.Count; multid++)
        {
            for (int pfid = 0; pfid < blockPrefabs.Length; pfid++)
            {
                menu.scoreBlockTextOut[multid][pfid].text = menu.scoreBlockCount[multid][pfid].ToString();
                textallid++;
            }
        }
    }
    public void blackoutCamera(bool start) { // prevent lag sickness during load screens
        if (start)
        {
            // fbtMgr.headCamera.farClipPlane = .02F;
            fbtMgr.headCamera.clearFlags = CameraClearFlags.Color;
            fbtMgr.headCamera.cullingMask = menu.maskLoading;
            menu.loadingScreen.SetActive(true);
        }
        else
        {

            // fbtMgr.headCamera.farClipPlane = 1000;
            fbtMgr.headCamera.clearFlags = CameraClearFlags.Skybox;
            fbtMgr.headCamera.cullingMask = menu.maskNormal;
            menu.loadingScreen.SetActive(false);
        }
    }
    public void scoreReset()
    {
        points = 0;
        pointFailureStatus = 200;
        foreach (Text t in menu.scoreViews) t.text = points.ToString();
        for (int multid = 0; multid < menu.scoreMultIndexes.Count; multid++)
        {
            for (int pfid = 0; pfid < blockPrefabs.Length; pfid++)
            {
                menu.scoreBlockCount[multid][pfid] = 0;
            }
        }
        if (paused) scoreUpdateInMenu();
    }
    public void saveHighScore()
    {
        menu.savedHighScore = true;       

        // dir
        string destination = Application.persistentDataPath + "/highscores/" + levelName + ".json";

        // create dir if it doesn't exist
        if (!Directory.Exists(Application.persistentDataPath + "/levels/")) Directory.CreateDirectory(Application.persistentDataPath + "/levels/");
                
        highScoreList highScores = new highScoreList();
        // load old scores if exist
        if (File.Exists(destination))
        {
            // Open save
            StreamReader sr = new StreamReader(destination);
            highScores = JsonUtility.FromJson<highScoreList>(sr.ReadToEnd());
            sr.Close();
        }

        highScores.Add(new highScore(points, "anonymous", menu.isGodMode));

        highScores.Sort();

        StreamWriter sw = new StreamWriter(destination, false);
        
        // write save data as json
        sw.Write(JsonUtility.ToJson(highScores));
        sw.Close();
        
        Debug.Log("Saved highscore to " + destination);
        displayHighScores(highScores);
    }
    public void loadHighScores()
    {
        // dir
        string destination = Application.persistentDataPath + "/highscores/" + levelName + ".json";

        // create dir if it doesn't exist
        if (!Directory.Exists(Application.persistentDataPath + "/levels/")) Directory.CreateDirectory(Application.persistentDataPath + "/levels/");

        
        // load old scores if exist
        if (File.Exists(destination))
        {
            // Open save
            StreamReader sr = new StreamReader(destination);
            highScoreList highScores = JsonUtility.FromJson<highScoreList>(sr.ReadToEnd());
            sr.Close();
            displayHighScores(highScores);
        }
        else
        {
            Debug.Log("No high scores found for " +destination);
        }
    }
    public void displayHighScores(highScoreList highScores)
    {
        foreach (Transform child in menu.highscoreTemplate.transform.parent)
        {
            if (child.name != "DefaultHighScore") Destroy(child.gameObject);
        }
        foreach (highScore hs in highScores.list)
        {
            // create load buttons
            GameObject ns = Instantiate(menu.highscoreTemplate, menu.highscoreTemplate.transform.parent);
            ns.SetActive(true);
            ns.name = hs.time;
            ns.transform.Find("score").GetComponent<Text>().text = hs.score.ToString();
            ns.transform.Find("time").GetComponent<Text>().text = hs.time;
            ns.transform.Find("godmode").gameObject.SetActive(hs.godmode);
            if (hs.score == points) ns.GetComponent<Image>().color = Color.green;
        }
    }
    public void setGodMode(bool set)
    {
        if(menu.isGodMode != set)
        {
            menu.isGodMode = set;
            scoreReset();
            
        }
    }
    public void tapForBPM()
    {
        if (music.clip != null)
        {
            if (menumusic.clip.name != music.clip.name) menumusic.clip = music.clip;
            if (menumusic.clip.length > 8 && !menumusic.isPlaying)
            {
                menumusic.time = 8;
                menumusic.Play();

                StartCoroutine(endMenuMusic());
            }
            //StopCoroutine("endMenuMusic");
        }
        if (menu.bpmCount.Count >= 1)
        {
            Debug.Log("time: " + (Time.unscaledTime - menu.bpmCount[menu.bpmCount.Count - 1])+" out of count: "+menu.bpmCount.Count);
            if (Mathf.Abs(Time.unscaledTime - menu.bpmCount[menu.bpmCount.Count-1]) > 5)
            {
                menu.bpmCount.Clear();
                menu.bpmCount.Add(Time.unscaledTime);
            }
            else
            {
                menu.bpmCount.Add(Time.unscaledTime);
                float total = 0;
                for(int i=1; i < menu.bpmCount.Count; i++)
                {
                    total += menu.bpmCount[i] - menu.bpmCount[i - 1];
                }
                bpm = Mathf.Floor(1/(total / (menu.bpmCount.Count - 1)) * 60);
                menu.bpm.text = bpm.ToString();
            }
        }
        else
        {
            menu.bpmCount.Add(Time.unscaledTime);
        }
    }
    IEnumerator endMenuMusic()
    {
        yield return new WaitForSecondsRealtime (10);
        menumusic.Stop();
    }
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 0; //ensure paused

        // get startuppath (test this crossplatform if i ever support other platforms)
        startupPath = System.IO.Directory.GetCurrentDirectory();

        // create scoreBlockCount lists (UI)
        menu.scoreMultIndexes[4] = 0;
        menu.scoreMultIndexesReverse[0] = 4;
        menu.scoreMultIndexes[2] = 1;
        menu.scoreMultIndexesReverse[1] = 2;
        menu.scoreMultIndexes[1] = 2;
        menu.scoreMultIndexesReverse[2] = 1;
        menu.scoreMultIndexes[-4] =3;
        menu.scoreMultIndexesReverse[3] = -4;
        menu.scoreMultIndexes[-8] = 4;
        menu.scoreMultIndexesReverse[4] = -8;
        menu.scoreMultIndexes[1000] = 5;
        menu.scoreMultIndexesReverse[5] = 0;
        List<Text> tmpScoreTextAll = new List<Text>();
        foreach (Transform child in menu.scoreGrid)
        {
            if (child.name == "score")
            {
                Text tmp = child.GetComponentInChildren<Text>();
                if (tmp != null)
                {
                    tmpScoreTextAll.Add(tmp);
                }
            }
        }
        int textallid = 0;
        for (int multid = 0; multid < menu.scoreMultIndexes.Count; multid++)
        {
            List<int> tmpIndexList = new List<int>();
            List<Text> tmpTextList = new List<Text>();
            for (int pfid = 0; pfid < blockPrefabs.Length; pfid++)
            {
                tmpIndexList.Add(0);
                tmpTextList.Add(tmpScoreTextAll[textallid]);
                textallid++;
            }
            menu.scoreBlockCount.Add(tmpIndexList);
            menu.scoreBlockTextOut.Add(tmpTextList);
        }
        menu.downIndicatoranim = menu.downIndicator.GetComponent<Animator>();

        // create folders on first launch
        if (!Directory.Exists(Application.persistentDataPath + "/levels/")) Directory.CreateDirectory(Application.persistentDataPath + "/levels/");
        if (!Directory.Exists(startupPath + "/levels/")) Directory.CreateDirectory(startupPath + "/levels/");
        if (!Directory.Exists(Application.persistentDataPath + "/songs/")) Directory.CreateDirectory(Application.persistentDataPath + "/songs/");
        if (!Directory.Exists(startupPath + "/songs/")) Directory.CreateDirectory(startupPath + "/songs/");
        if (!Directory.Exists(Application.persistentDataPath + "/highscores/")) Directory.CreateDirectory(Application.persistentDataPath + "/highscores/");
        if (!Directory.Exists(startupPath + "/highscores/")) Directory.CreateDirectory(startupPath + "/highscores/");
        
        // scan for levels
        GenerateListofLevelsandSongs();

        // show fps
        StartCoroutine(showFPSUpdate());
    }


    public void GenerateListofLevelsandSongs()
    {
        float startRefreshTime = Time.unscaledTime;
        // Destroy old lists
        foreach (Transform child in menu.saveList)
        {
            if (child.name != "DefaultSongLoadButton") Destroy(child.gameObject);
        }
        foreach (Transform child in menu.songList)
        {
            if(child.name != "DefaultSongFileLoadButton") Destroy(child.gameObject);
        }

        // Get Loaded Levels
        List<string> levelFiles = new List<string>();
        levelFiles.AddRange(Directory.GetFiles(startupPath + "/levels/", "*.json"));
        levelFiles.Reverse(); // me being anal about order of default songs
        levelFiles.AddRange(Directory.GetFiles(Application.persistentDataPath + "/levels/", "*.json"));


        // eliminate doubles (later files in list overwrite earlier files, so that user-edited content is preferred
        levelFiles.Reverse();
        List<string> levelFilesNoDuplicates = new List<string>();
        foreach (string lf in levelFiles)
        {
            bool isDuplicate = false;
            foreach (string lf2 in levelFilesNoDuplicates)
            {
                if (lf.Split(new string[] { "levels" }, System.StringSplitOptions.None).Last() == lf2.Split(new string[] { "levels" }, System.StringSplitOptions.None).Last())
                {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) levelFilesNoDuplicates.Add(lf);
        }

        foreach (string lf in levelFilesNoDuplicates)
        {
            // read json
            StreamReader sr = new StreamReader(lf);
            songSave sav = JsonUtility.FromJson<songSave>(sr.ReadToEnd());
            sr.Close();            
            // create load buttons
            GameObject ns = Instantiate(menu.saveLoadButtonTemplate, menu.saveList);
            ns.SetActive(true);
            ns.name = sav.levelName;            
            ns.GetComponent<Button>().onClick.AddListener(delegate { LoadLevelByButton(lf); });
            ns.transform.Find("songname").GetComponent<Text>().text = sav.songName;            
            ns.transform.Find("a/author").GetComponent<Text>().text = sav.author;
            ns.transform.Find("b/bpm").GetComponent<Text>().text = sav.bpm.ToString();
            ns.transform.Find("d/difficulty").GetComponent<Text>().text = sav.difficulty;
            ns.transform.Find("l/length").GetComponent<Text>().text = System.TimeSpan.FromSeconds(Mathf.Floor(sav.duration)).ToString();
            if (!lf.Contains(startupPath))
            {
                ns.transform.Find("default").gameObject.SetActive(false);
            }
        }
        //menu.scrLoad.verticalNormalizedPosition = 0;
        // Get Song Files
        List<string> songFiles = new List<string>();
        songFiles.AddRange(Directory.GetFiles(startupPath + "/songs/", "*.*"));
        songFiles.AddRange(Directory.GetFiles(Application.persistentDataPath + "/songs/", "*.*"));
        foreach (string sf in songFiles)
        {
            // get filename
            string songfile = sf.Replace(Application.persistentDataPath + "/songs/", "");
            songfile = songfile.Replace(startupPath + "/songs/", "");
            string songtitle = songfile.Replace(".ogg", "");
            songtitle = songtitle.Replace(".mp3", "");
            // create load buttons
            GameObject ns = Instantiate(menu.songLoadButtonTemplate, menu.songList);
            ns.SetActive(true);
            RectTransform nst = ns.GetComponent<RectTransform>();

            ns.name = songtitle;
            ns.GetComponent<Button>().onClick.AddListener(delegate { LoadSongOnly(sf, songtitle, songfile); });
            ns.transform.Find("songname").GetComponent<Text>().text = songtitle;
            if (!sf.Contains(startupPath))
            {
                ns.transform.Find("default").gameObject.SetActive(false);
            }
        }
        //menu.scrSong.verticalNormalizedPosition = 0;

        Debug.Log("Refresh Level/Song List: "+ levelFilesNoDuplicates.Count.ToString() + " levels loaded, "+songFiles.Count.ToString()+" Songs loaded. Ignored "+ (levelFiles.Count() - levelFilesNoDuplicates.Count()) + " duplicates. Took " +(Time.unscaledTime - startRefreshTime).ToString()+" Seconds.");
    }
    
    // Update is called once per frame
    void Update()
    {
        //Debug.Log("time:"+music.time.ToString()+", timesamples:"+music.timeSamples.ToString());
        // DEBUG STUFF
        if (tmpTest)
        {
            tmpTest = false;
            if(tmp2 == 2)
            {
                tmp2 = 1;
                //fbtMgr.applyFBTOffsets();
                foreach (block b in blocks) b.debugForceAwake = true;
                SetMode(gameMode.edit, true, true);
            }
            else
            {
                tmp2 = 2;
                scrubSongTime(music.clip.length - 3);
                SetMode(gameMode.edit, false, false);
                //fbtMgr.resetRoomCenterAndRotation();
                //fbtMgr.resetFBTOffsets();
            }
        }
        if (tmpTest2)
        {
            tmpTest2 = false;
            pauseToggle();
            
        }
        if (doSave)
        {
            if (blocks.Count > 0)
            {
                doSave = false;
                SaveLevel();
            }
        }
        if (doLoad)
        {
            doLoad = false;
            menu.isGodMode = true;
            menu.audioVisualizationEnabled = true;
            LoadLevel("C:\\Users\\Bucket\\AppData\\LocalLow\\Teh_Bucket\\Full_Body_Rhythm\\levels\\Stefon1A - Ding Dong.json",true);
            blocks[8].isDebugBlock = true;
        }
        if (doLoadsng)
        {
            doLoadsng = false;
            LoadSongOnly("C:\\Users\\Bucket\\AppData\\LocalLow\\Teh_Bucket\\Full_Body_Rhythm\\songs\\Fupi - Elevator.ogg", "Fupi - Elevator.ogg", "Fupi - Elevator.ogg");
        }

        // pause game if steam menu
        if(!paused && Time.timeScale == 0)
        {
            SetMode(mode, false, true);
        }

        // song finished
        if(!paused && !music.isPlaying)
        {
            if(mode == gameMode.play)
            {
                saveHighScore();
                SetMode(gameMode.play, false, true);
                menu.succeeded.SetActive(true);
            }
            else if(mode == gameMode.edit)
            {
                Debug.Log("edit song ended-----");
                SetMode(gameMode.edit, true, true);
                scrubSongTime(music.clip.length-(4*60/bpm)); // go to near the end of the song for further editing, and avoid music-unload glitch
            }
        }

        // fps
        menu.fps = Mathf.Floor( 1.0F / Time.unscaledDeltaTime);

        // audio visualization skybox
        if (menu.audioVisualizationEnabled && mode == gameMode.play && !paused)
        {
            int detail = 64;
            float[] spectrum = new float[detail];
            music.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
            int lowThreshold = 16;
            int midThreshold = 48;
            float lowAvg = 0;
            float midAvg = 0;
            float highAvg = 0;
            float bps = 1/(bpm / 120);
            float bpmNear = Mathf.Abs(((music.time % bps) / bps) - bps / 2);

            for (int i = 0; i < lowThreshold; i++) lowAvg += spectrum[i] / lowThreshold;
            for (int i = lowThreshold; i < midThreshold; i++) midAvg += spectrum[i] / (midThreshold - lowThreshold);
            for (int i = midThreshold; i < detail; i++) highAvg += spectrum[i] / (detail - midThreshold);
            RenderSettings.skybox.SetFloat("_SunSize", lowAvg * 3);
            if(bpm > 0)
            {
                RenderSettings.skybox.SetFloat("_AtmosphereThickness", bpmNear);
            }
            RenderSettings.skybox.SetColor("_SkyTint", menu.rotorscape.material.GetColor("Color_5653FE04"));
            //RenderSettings.skybox.SetFloat("_Exposure", highAvg*tmp2);            
            //Debug.Log(lowAvg.ToString() + ", " + midAvg.ToString() + ", " + highAvg.ToString() + " and: " + bpmNear);
        }
    }

    // SAVE
    public void SaveLevel()
    {
        debugTextOut(menu.saveDebugOut, "Writing file...");

        // load text fields
        

        // prevent empty fields
        if(levelName.Length == 0)
        {
            levelName = "untitled" + Random.Range(1000, 9999).ToString();
        }
        if(songName.Length == 0)
        {
            songName = levelName;
        }
        if(songFileName.Length == 0)
        {
            songFileName = songFileName + ".ogg";
        }

        // save
        string destination = Application.persistentDataPath + "/levels/"+levelName+".json";       

        // create dir if it doesn't exist
        if (!Directory.Exists(Application.persistentDataPath + "/levels/")) Directory.CreateDirectory(Application.persistentDataPath + "/levels/");


        //if (File.Exists(destination)) file = File.OpenWrite(destination);
        //else file = File.Create(destination);

        StreamWriter sw = new StreamWriter(destination, false);

        // translate save data
        List<blockSave> blockSaves = new List<blockSave>();
        foreach(block n in blocks)
        {
            if (n.transform.parent.name == "songBlocks")
            {
                blockSaves.Add(n.saveBlockData());
            }
        }
        songSave data = new songSave(levelName, songName, songFileName, author, stageFileName, difficulty, bpm, music.clip.length, blockSaves);

        // write save data as json
        sw.Write(JsonUtility.ToJson(data));
        sw.Close();

        //BinaryFormatter bf = new BinaryFormatter();
        //bf.Serialize(file, data);


        //file.Close();
        Debug.Log("Saved level to " + destination);
        debugTextOut(menu.saveDebugOut, "Saved Successfully");
    }

    // LOAD
    public void LoadLevel(string saveName, bool fullPath = false)
    {
        debugTextOut( menu.loadDebugOut, "Loading Level...");
        
        string destination = saveName;
        if (!fullPath)
        {
            destination = Application.persistentDataPath + "/levels/" + levelName + ".json";
        }
        

        if (!File.Exists(destination))
        {
            debugTextOut( menu.loadDebugOut, "File not found!");
            Debug.LogError("File not found: "+destination);
            return;
        }

        // Pause song, enter edit mode
        blackoutCamera(true);
        
        isStarted = true;
        //SetMode(gameMode.play,true, true);

        // Open save
        StreamReader sr = new StreamReader(destination);
        songSave sav = JsonUtility.FromJson<songSave>(sr.ReadToEnd());
        sr.Close();

        // general vars
        levelName = sav.levelName;
        songName = sav.songName;
        songFileName = sav.songFileName;
        author = sav.author;
        stageFileName = sav.stageFileName;
        difficulty = sav.difficulty;
        bpm = sav.bpm;

        // update inputField data
        menu.levelName.text = levelName;
        menu.songName.text = songName;
        foreach (Text snv in menu.songNameViews) snv.text = songName;
        menu.songFileName.text = songFileName;
        menu.author.text = author;
        menu.difficulty.text = difficulty;
        menu.bpm.text = bpm.ToString();

        deleteAllBlocks(true);

        // place saved blocks
        foreach (blockSave n in sav.blocks)
        {
            if (n.moveDir != Vector3.zero && n.spawnTime > 0)
            {
                GameObject nb = Instantiate(blockPrefabs[n.prefabID], n.placePos - ((Vector3)n.moveDir * n.spawnTime * 15), (Quaternion)n.placeRot);
                block ns = nb.GetComponent<block>();                
                blocks.Add(ns);
                ns.loadBlockData(n);
                ns.songMgr = this;
                ns.music = music;
                ns.StartPostSpawn(0, bpm);
                if (!menu.showBlockAnim) ns.toggleAnim(false);
                if (!menu.showBlockTrails) ns.toggleTrail(false);
                if (menu.showBlockColorblind) ns.toggleColorblind(true, false);
                else if (!menu.showBlockShaders) ns.toggleShader(false);
                nb.transform.parent = songBlocks.transform;
                ns.isEditMode = false;
            }
        }
        // Load song
        StartCoroutine(LoadAudioFile(Application.persistentDataPath+"/songs/"+ songFileName)); // function will check both paths
        

        // load highscores
        loadHighScores();
    }

    IEnumerator LoadAudioFile(string filepath)
    {
        blackoutCamera(true);
        yield return new WaitForEndOfFrame();
        menu.saveDebugOut.gameObject.SetActive(false);
        debugTextOut( menu.loadDebugOut, "Loading song...");
        bool fileExists = false;
        if (!File.Exists(filepath))
        {
            Debug.Log("audio file doesn't exist in: " + filepath);
            filepath = filepath.Replace(Application.persistentDataPath, startupPath);
            Debug.Log("checking instead if audio file exists in: " + filepath);
            if (File.Exists(filepath)) fileExists = true;
        }
        else fileExists = true;
        if (fileExists) { 
            AudioType audiotype = AudioType.OGGVORBIS;

            string ext = filepath.Split('.')[filepath.Split('.').Length - 1].ToLower();
            if (ext == "ogg")
            {
                audiotype = AudioType.OGGVORBIS;
            }
            else if (ext == "mp3")
            {
                audiotype = AudioType.MPEG;
            }
            else if (ext == "wav")
            {
                audiotype = AudioType.WAV;
            }
            else if (ext == "aiff")
            {
                audiotype = AudioType.AIFF;
            }
            else
            {
                Debug.Log("Error: Unsupported Audio Filetype: " + ext);
                audiotype = AudioType.UNKNOWN;
                blackoutCamera(false);
            }

            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(filepath, audiotype))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    Debug.Log(www.error);

                    debugTextOut(menu.loadDebugOut, "FAILED loading song file (E1)");
                }
                else
                {
                    AudioClip clip = DownloadHandlerAudioClip.GetContent(www);

                    if (clip != null)
                    {

                        music.clip = clip;
                        audioLoaded = true;
                        Debug.Log("Finished loading " + filepath);
                        menumusic.Stop();
                        debugTextOut(menu.loadDebugOut, "Loaded Song Successfuly.");

                        // start game on timer to allow song to load to avoid desync? maybe?
                        StartCoroutine(startGameOnDelay(.1F));
                    }
                    else
                    {
                        debugTextOut(menu.loadDebugOut, "ERROR Loading Song File (E2)");
                        deleteAllBlocks(true);
                        blackoutCamera(false);
                    }
                }
            }
        }
        else
        {
            debugTextOut(menu.loadDebugOut, "Error: Audio File Not Found.");
            Debug.LogError("Audio file not found: " + filepath);
            yield return null;

            blackoutCamera(false);
        }
        //blackoutCamera(false);
    }

    IEnumerator startGameOnDelay(float delay)
    {
        gameMode doMode;
        if (blocks.Count>0) doMode = gameMode.play;
        else doMode = gameMode.edit;
        //SetMode(doMode, true, true);
        yield return new WaitForSecondsRealtime(delay);
        SetMode(doMode, true, false);
    }

    void applyModeChangeExternally(bool isEditMode, bool isPaused)
    {
        foreach (controllerGeneric handgeneric in fbtMgr.hands)
        {
            controllerRhythm hand = handgeneric.GetComponent<controllerRhythm>();
            hand.changeMode(isEditMode, isPaused);
        }
        sceneAnim.SetBool("isEdit", isEditMode);
        sceneAnim.SetBool("isPaused", isPaused);
    }

    void debugTextOut(Text obj, string str)
    {
        obj.text = str;
        obj.gameObject.SetActive(true);
    }

}
