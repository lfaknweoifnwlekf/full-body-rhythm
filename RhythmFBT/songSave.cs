﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

[System.Serializable]
public class songSave
{


    [Header("Save Info")]
    public string levelName;
    // Name of level and save file. Visible ingame. Saves over existing files with same name
    public string songName;
    // Name of song / artist / etc. Purely cosmetic. Visible ingame
    public string songFileName;
    // Audio filename, including extension, to load with level. Audio file must be included in the same directory
    public string author = "Anonymous";
    // Your name. Visible ingame. Purely cosmetic
    public string stageFileName;
    // (DOESNT EXIST YET) Unitypackage filename, including extension, to load with level. Background effects and such. Must be created in unity (see docs)
    public string difficulty;
    // Description of song difficulty, playspace size, etc. Purely cosmetic, visible ingame
    public float bpm = 100;
    // Beats Per Minute: if set, will try to sync some ingame visuals to the song's bpm. Number only
    public float duration = 0;
    // song length. cosmetic only (will be reset on load)

    [Header("Save Data")]
    // Level's block data. Good luck editing manually. Some tips: prefabID's refer to which block it is, 0-8. So in order: (hand block, foot block, hip block, hand grind, foot grind, hip grind, dodge block, foot tap pad). Minforce and limbid are deprecated
    public List<blockSave> blocks;

    public songSave(string levelName_, string songName_, string songFileName_, string author_, string stageFileName_, string difficulty_, float bpm_, float duration_, List<blockSave> blocks_){
        levelName = levelName_;
        songName = songName_;
        songFileName = songFileName_;
        stageFileName = stageFileName_;
        author = author_;
        blocks = blocks_;
        difficulty = difficulty_;
        bpm = bpm_;
        duration = duration_;
        //song = Convert.ToBase64String (song_.GetData(song_.samples,0));
    }
}


[System.Serializable]
public class highScore
{
    public int score;
    public string time;
    public string name;
    public bool godmode;
    
    public highScore(int score_, string name_= "Anonymous", bool godmode_ = false)
    {
        score = score_;
        time = DateTime.Now.ToString();
        name = name_;
        godmode = godmode_;
    }
}


[System.Serializable]
public class highScoreList
{
    public List<highScore> list = new List<highScore>();

    public highScoreList()
    {
        list = new List<highScore>();
    }
    public highScoreList(List<highScore> list_)
    {
        list = list_;
    }
    public highScoreList(highScore score_)
    {
        list = new List<highScore>();
        list.Add(score_);
    }
    public void Add(highScore score_)
    {
        list.Add(score_);
    }
    public int Count()
    {
        return list.Count;
    }
    public void Sort()
    {
        list.Sort((x, y) => y.score.CompareTo(x.score));
    }
}
