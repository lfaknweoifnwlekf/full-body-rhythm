﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


/* Tracks how strongly/fastish this tracker has been moving. Determines if blocks can be hit. Very janky. */
public class trackerPower : MonoBehaviour
{
    [Header("References")]
    private Animator anim;
    public Renderer chargeRenderer;
    

    [Header("Public Stuff")]
    public float chargedAmount = 0;
    public float swingForceMultiplier = 1;
    private List<Vector3> swingHistory;
    public float swingHistoryDelayDuration = .1F;
    private bool timePassed = true;
    private Vector3 swingStart;
    private float swingStartTime;
    private Vector3 swingEnd;
    public Vector3 swingDir;
    private Vector3 swingLastFrame;    
    public float swingPower;
    public float minSwingPower = .1F; //distance per second to be considered moving
    public float maxHesitationLength = .1F; // grace period between movements (otherwise it keeps glitching on and off active)
    public float hesitationLength = 10;
    [Tooltip("How wide can a swing deviate before being considered a new swing")] public float maxAngle = 30F;
    private Vector3 spawnPos;
    private Vector3 spawnOffset;    
    private Quaternion spawnRot;
    private Vector3 spawnPosVisual;
    private Quaternion spawnRotVisual;

    [SerializeField] private bool debugPower = false;



    // Start is called before the first frame update
    void Start()
    {
        swingHistory = new List<Vector3>();
        
        spawnOffset = transform.localPosition;
        spawnPos = spawnOffset;
        spawnRot = transform.rotation;

        spawnPosVisual = chargeRenderer.transform.localPosition;
        spawnRotVisual = chargeRenderer.transform.localRotation;

        anim = GetComponent<Animator>();

        
    }

    // Update is called once per frame
    void Update()
    {
        float swingPowerThisFrame = Vector3.Distance(transform.position, swingLastFrame) / Time.deltaTime;
        bool swingHistoryPass = true;
        foreach(Vector3 n in swingHistory)
        {
             if (maxAngle < Vector3.Angle(transform.position - n, (transform.position - swingStart))) swingHistoryPass = false;
        }
        if ((swingPowerThisFrame > minSwingPower && maxAngle > Vector3.Angle(swingDir, (transform.position - swingStart)) && swingHistoryPass))
        {
            //swingDir += transform.position - swingLastFrame;
            swingDir = transform.position - swingStart;

            // TODO - cancel swingDir on reverse direction somehow (measure angle)
            //swingPower += swingPowerThisFrame;
            swingPower = Vector3.Distance(transform.position,swingStart) / (Time.timeSinceLevelLoad - swingStartTime);
            chargedAmount += Time.deltaTime;
            // visuals
            Debug.DrawRay(transform.position, swingDir*swingPower,Color.red,.05F);
            Debug.DrawRay(swingStart, swingDir, Color.yellow);
            if (anim != null)
            {
                anim.SetBool("isPowered", true);
            }
            if (timePassed)
            {
                timePassed = false;
                StartCoroutine(timePassDelay());
                swingHistory.Add(transform.position);
            }
            hesitationLength = 0;
        }
        else
        { // swing is over
            swingStart = transform.position;
            swingStartTime = Time.timeSinceLevelLoad;
            if (swingPower != 0)
            {
                if (hesitationLength < maxHesitationLength)
                {     // don't penalize power until hesitation reaches max
                    hesitationLength += Time.deltaTime;
                }
                else
                { // reset swing entirely
                    swingPower = 0;
                    chargedAmount = 0;
                    // visuals
                    if (anim != null)
                    {
                        anim.SetBool("isPowered", false);
                    }
                }
                swingDir = Vector3.zero;
                timePassed = true;
                swingHistory.Clear();
            }
        }
        swingLastFrame = transform.position;
        if (debugPower) Debug.Log("SwingPower: "+ swingPower);
    }

    IEnumerator timePassDelay()
    {
        yield return new WaitForSeconds(swingHistoryDelayDuration);
        timePassed = true;
    }

    public void resetTracker(Vector3 center)
    {
        GetComponent<SteamVR_TrackedObject>().SetDeviceIndex(-1);
        transform.position= new Vector3(center.x,0,center.z) + spawnPos;
        transform.rotation = Quaternion.identity;
        chargeRenderer.transform.localPosition = spawnPosVisual;
        chargeRenderer.transform.localRotation = spawnRotVisual;
    }
    public void applyTracker()
    {
        //spawnPos = transform.position;
        //spawnRot = transform.rotation;
    }
}
