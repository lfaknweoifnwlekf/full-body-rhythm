﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

// One script on each hand. Each refers to corresponding foot.
public class controllerRoller : controllerGeneric
{    
    [Header("Settings")]
    [SerializeField]
    private float jetstrength;

    
    private trackerPower handPower;
    private trackerPower footPower;
    private trackerPower hipPower;

    private playerRoller player;

    public enum limbModes
    {
        none=0,
        grab=1,
        jet=2,
        shoot=3,
        more=4,
    }

    private limbModes handMode;
    private limbModes footMode;
    private bool handLocked;
    private bool footLocked;

    protected enum padZones
    {
        up = 3,
        down = 2,
        left = 4,
        right = 1,
        none = 0
    }
    private padZones padZone;
    private padZones oldPadZone;


    public float padDeadzone = .1F;

    // Use this for initialization
    void Start()
    {
        genericStart();

        handPower = GetComponent<trackerPower>();
        footPower = foot.GetComponent<trackerPower>();
        hipPower = hip.GetComponent<trackerPower>();

        player = transform.parent.GetComponent<playerRoller>();
        // block visuals (editor)

    }

    
    // Update is called once per frame
    void Update()
    {
        // update padZone (where is thumb on the touchpad: SQUARE DEADZONE (fix with math TODO))
        if (padTouch.GetState(trackedObj.inputSource)) { 
            padDir = padPos.GetAxis(trackedObj.inputSource);
            padClickedDown = padClick.GetStateDown(trackedObj.inputSource);
            padTouched = true;
            oldPadZone = padZone;
            if (padTouchDistance < padDeadzone)
            {
                padZone = padZones.none;
            }
            else if (padAngle < -135 || padAngle > 135)
            {
                padZone = padZones.down;
            }
            else if (padAngle < -45)
            {
                padZone = padZones.left;
            }
            else if (padAngle < 45)
            {
                padZone = padZones.up;
            }
            else
            {
                padZone = padZones.right;
            }
            handMode = (limbModes) padZone;
        }
        else
        {
        }

        // Spawn blocks (edit mode)
        if (!isPauseMenu)
        {
            // Punch block
            if (button1.GetStateDown(trackedObj.inputSource))
            {
                //rigid.AddForce(hand.transform.forward * jetstrength * Time.deltaTime);
            }
            
            // Kick block
            if (button2.GetStateDown(trackedObj.inputSource))
            {
                //rigid.AddForce(-foot.transform.up* jetstrength * Time.deltaTime);
            }

            // change modes
            if (padTouched && padZones.up == padZone)
            {
                //handMode = limbModes.grab;
            }
        }
        // pause menu (laser pointer)
        if (isPauseMenu)
        {
            if (isPauseMenuPointer)
            {
                laserVisual.gameObject.SetActive(true);

                Ray raycast = new Ray(laserMuzzle.position, laserMuzzle.forward);
                anim.SetBool("pauseGrip", true);
                RaycastHit hit;
                bool rayHit;
                //if(songMgr.isDeleteMode) rayHit = Physics.Raycast(raycast, out hit, 50, laserMask, QueryTriggerInteraction.Collide);
                rayHit = Physics.Raycast(raycast, out hit, 50, laserMaskMenuOnly);
                Debug.DrawRay(laserMuzzle.position, laserMuzzle.forward, Color.red);
                if (rayHit)
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, hit.point);

                    Button but = hit.transform.GetComponent<Button>();
                    Toggle tog = hit.transform.GetComponent<Toggle>();
                    InputField inp = hit.transform.GetComponent<InputField>();
                    if (but != null)
                    {
                        EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            but.onClick.Invoke();
                        }
                    }
                    else if(tog != null)
                    {
                        EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            tog.isOn = !tog.isOn;
                        }
                    }
                    else if(inp != null)
                    { 
                        if (button1.GetStateDown(trackedObj.inputSource))
                        {
                            EventSystem.current.SetSelectedGameObject(hit.transform.gameObject);
                        }
                    }
                    else if(hit.transform.name == "scrollbar-song")
                    {

                        if (button1.GetState(trackedObj.inputSource))
                        {
                            // song scrolling (dont want to translate 2d ui position so its hard coded)
                            Vector3[] corners = new Vector3[4];
                            hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                            float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[3]);
                            //songMgr.scrubSongTime(songMgr.music.clip.length * dist - songMgr.music.time);
                        }
                    }
                    else if(hit.transform.name == "Scrollbar Vertical")
                    {
                        // song scrolling (dont want to translate 2d ui position so its hard coded)
                        Vector3[] corners = new Vector3[4];
                        hit.transform.GetComponent<RectTransform>().GetWorldCorners(corners);
                        float dist = Vector3.Distance(corners[0], hit.point) / Vector3.Distance(corners[0], corners[2]);
                        hit.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = dist;
                    }

                    // delete mode
                    if (hit.transform.tag == "block")
                    {
                    }
                    else
                    {
                        if (laserVisColor != 1)
                        {
                            laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.green);
                            laserVisColor = 1;
                        }
                    }
                }
                else
                {
                    laserVisual.SetPosition(0, laserMuzzle.position);
                    laserVisual.SetPosition(1, laserMuzzle.position + laserMuzzle.forward * 50);
                    anim.SetBool("pauseGrip", false);
                    if (laserVisColor != 0)
                    {
                        laserVisual.GetComponent<Renderer>().material.SetColor("Color_5653FE04", Color.white);
                        laserVisColor = 0;
                    }
                }
            }
            else
            {
                laserVisual.gameObject.SetActive(false);
            }

            // pausemode trackpad
            if (padClickedDown && padZone == padZones.none)
            {
            }
            if (padClickedDown && padZone == padZones.up)
            {
            }
            if(padTouched && padZone == padZones.up)
            {
            }
            else if (padTouched && padZone == padZones.down)
            {
            }

        }
        else
        {
            laserVisual.gameObject.SetActive(false);
        }
        
        // pause/unpause (toggles menu)
        if (menu.GetStateDown(trackedObj.inputSource))
        {
        }
    }

}
