﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerRoller : MonoBehaviour {

    [Header("Stuff")]
    public float pushPower = 1;
    public float groundedDeadzoneHeight = .02F;
    public float jumpPower = 100;
    [Tooltip("The minimum friction when rolling with perfect pose")] public float minFriction = 0.01F;
    [Tooltip("The friction when rolling with the worst pose (braking)")] public float maxFriction = 10F; //actually half of max
    [Tooltip("portion of a 180 that can be done per second, assuming a perfect turning pose")] public float maxTurnMod = 1;


    [Header("References")]    
    public Transform leftFoot;    
    public Transform rightFoot;
    private trackerPower leftFootPower;
    private trackerPower rightFootPower;

    // data
    private bool groundedLeftFoot = true;
    private bool groundedRightFoot = true;
    [SerializeField] private int groundedFeet = 2;
    [SerializeField] private float feetAngle = 0;
    private float feetPushPower = 0;
    private Vector3 feetPushDir = Vector3.zero;
    private Vector3 turnModDir = Vector3.zero;
    [SerializeField] private float turnMod = 0; // how well is the player leaning in a direction for a turn, in degrees, 0 being straight ahead
    [SerializeField] private Vector3 feetDir = Vector3.zero;
    [SerializeField] private float feetDirDifference = 0; // angle between feetDir and velocity dir
    private bool isHooked = false;

    private Rigidbody rigid;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        leftFootPower = leftFoot.GetComponent<trackerPower>();
        rightFootPower = rightFoot.GetComponent<trackerPower>();
    }

    void FixedUpdate()
    {
        // gather data        
        groundedLeftFoot = (leftFoot.localPosition.y <= groundedDeadzoneHeight);
        groundedRightFoot = (rightFoot.localPosition.y <= groundedDeadzoneHeight);
        groundedFeet = CountBools(groundedLeftFoot, groundedRightFoot);
        if(groundedFeet == 2) { 
            feetAngle = Mathf.Abs(leftFoot.localEulerAngles.y - rightFoot.localEulerAngles.y);
            feetDir = Vector3.Normalize( leftFoot.forward + rightFoot.forward);
            Debug.Log(leftFoot.localEulerAngles.y);
        }
        else if(groundedFeet == 1){
            feetAngle = 0;
            if (groundedLeftFoot) {
                feetDir = Vector3.Normalize(leftFoot.forward);
                feetPushPower = rightFootPower.swingPower;
                feetPushDir = new Vector3(rightFootPower.swingDir.x, 0, rightFootPower.swingDir.z);
            }
            else if (groundedRightFoot) {
                feetDir = Vector3.Normalize(rightFoot.forward);
                feetPushPower = leftFootPower.swingPower;
                feetPushDir = new Vector3(leftFootPower.swingDir.x, 0, leftFootPower.swingDir.z);
            }
        }
        else {
            feetAngle = 0;
            feetDirDifference = 0;
            feetDir = Vector3.Normalize(rigid.velocity);            
        }
        if(rigid.velocity.x < .01 && rigid.velocity.z < .01) {
            feetDirDifference = 0;
        }
        else {
            feetDirDifference = Vector3.Angle(new Vector3(feetDir.x, 0, feetDir.z), new Vector3(rigid.velocity.x, 0, rigid.velocity.z));
        }
        

        // determine friction
        if (groundedFeet > 0) {
            rigid.drag = (((feetAngle + feetDirDifference) / 360) * maxFriction) + minFriction; 
        }


        // redirect velocity towards feetDir
        if(groundedFeet >= 1 && feetDirDifference > 0 && !isHooked) {
            Vector3 lerpDir = Vector3.Lerp(rigid.velocity, feetDir, (((feetDirDifference/180)*-1) + 1) * maxTurnMod * Time.fixedDeltaTime); // TODO: add turnmod
            lerpDir = new Vector3(lerpDir.x, rigid.velocity.y, lerpDir.z);
            rigid.velocity = lerpDir;
        }

        // push (kick force)
        if (groundedFeet == 1 && feetPushPower > 0) {
            rigid.AddForce(-feetPushDir * feetPushPower * pushPower);
        }

        // jump (constant force)
        if (groundedFeet == 0) {
            rigid.AddForce(transform.up * jumpPower * Time.fixedDeltaTime);
        }


    }

    private void Update() {

        // debug
        Debug.DrawRay(transform.position, feetDir, Color.red);
        Debug.DrawRay(transform.position, rigid.velocity, Color.cyan);
        Debug.DrawRay(transform.position, feetPushDir * feetPushPower, Color.yellow);
    }

    public static int CountBools(params bool[] args) {
        int i = 0;
        foreach(bool arg in args) {
            if (arg) i++;
        }
        return i;
    }
}
