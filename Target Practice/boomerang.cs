﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


using Valve.VR.InteractionSystem;

public class boomerang : MonoBehaviour {

    public GameObject bullet;
    public GameObject muzzle;
    public Animator gunAnim;
    public GameObject chargeVisual;

    [Header("Projectile")]
    public float swingForceMultiplier = 1;

    private bool canFire = true;
    private float chargedAmount = 0;
    private Vector3 swingStart;
    private Vector3 swingEnd;
    private Vector3 swingDir;
    private Vector3 swingLastFrame;
    private float swingPower;
    public float minSwingPower = .1F; //distance per second to be considered moving

    // steamvr stuff
    public SteamVR_Action_Boolean trigger;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;
    bool placeholder = false;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (placeholder/*down*/) {
            swingPower = 0;
            swingStart = muzzle.transform.position;
            swingLastFrame = muzzle.transform.position;
            swingDir = Vector3.zero;
            gunAnim.SetBool("grip", true);
        }
        else if (placeholder/*trigger held*/) {
            float swingPowerThisFrame = Vector3.Distance(muzzle.transform.position, swingLastFrame) / Time.deltaTime;
            if (swingPowerThisFrame > minSwingPower) {
                swingDir += muzzle.transform.position - swingLastFrame;
                swingPower += swingPowerThisFrame;
                chargedAmount += Time.deltaTime;
                float visScale = Mathf.Clamp(swingPowerThisFrame / 10, 0, .2F);
                chargeVisual.transform.localScale = new Vector3(visScale, visScale, visScale);
                Debug.DrawRay(muzzle.transform.position, swingDir);
            }
            else {
                swingStart = muzzle.transform.position;
                swingDir = Vector3.zero;
                swingPower = 0;
                chargeVisual.transform.localScale = Vector3.zero;
            }
            swingLastFrame = muzzle.transform.position;
        }
        else if (placeholder/*up*/) {
            GameObject newBullet = Instantiate(bullet, muzzle.transform.position, muzzle.transform.rotation) as GameObject;
            //newBullet.transform.LookAt(newBullet.transform.position + swingDir);
            newBullet.transform.rotation = Quaternion.LookRotation(swingDir);
            newBullet.GetComponent<Rigidbody>().AddForce(swingDir * swingPower * swingForceMultiplier);
            chargedAmount = 0;
            chargeVisual.transform.localScale = Vector3.zero;
            gunAnim.SetBool("grip", false);
        }
    }
}
