﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class feet : MonoBehaviour
{
    [SerializeField]
    private float jetstrength;

    [Header("Stuff")]
    [SerializeField]
    private Rigidbody rigid;
    [SerializeField]
    private Transform ycollider;

    [Header("test vars")]
    [SerializeField]
    private bool testing = false;


    // steamvr stuff
    public SteamVR_Action_Boolean button2;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (button2.GetState(inputSource) || testing) {
            rigid.AddForce(transform.up * jetstrength * Time.deltaTime);
        }

        // Y collider- cement shoes, fixed y (playspace floor). prevents falling without physics hiccups.
        ycollider.position = new Vector3(transform.position.x, ycollider.position.y, transform.position.z);

    }

    

    //private void OnEnable() {
    //    if (button2 != null) {
    //        button2.AddOnChangeListener(OnGripPressedOrReleased, inputSource);
    //        //button2.AddOnChangeListener()
    //    }
    //}

    //private void OnDisable() {
    //    if (button2 != null) {
    //        button2.RemoveOnChangeListener(OnGripPressedOrReleased, inputSource);
    //    }
    //}

    ////private SteamVR_Action_Single.ChangeHandler 

    //private void OnGripPressedOrReleased(SteamVR_Action_Boolean actionIn, SteamVR_Input_Sources inputSource, float newValue) {

    //    Debug.Log("button2 was pressed or released: " + newValue);
    //}
}
