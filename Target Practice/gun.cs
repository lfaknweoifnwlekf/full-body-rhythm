﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


using Valve.VR.InteractionSystem;

public class gun : MonoBehaviour {

    public GameObject bullet;
    public GameObject muzzle;
    public GameObject chargeVisual;

    public float projectForce = 100;
    [Tooltip("adds this many times projectforce per second")]
    public float chargeSpeed = .5F; // 
    public float maxChargeAmount = 250;
    [Tooltip("Shots per second")]
    public float fireRate = 4;

    private bool canFire = true;
    private float chargedAmount = 0;
    private float lastShotTime = 0;

    private Material mat;

    // steamvr stuff
    public SteamVR_Action_Boolean trigger;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;
    bool placeholder = false;

    // Use this for initialization
    void Start() {
        mat = GetComponentInChildren<MeshRenderer>().material;
    }

    IEnumerator cooldown() {
        yield return new WaitForSeconds(1 / fireRate);
        canFire = true;
    }

    // Update is called once per frame
    void Update() {
        if ( canFire && placeholder/*held*/) {
            if (chargedAmount < maxChargeAmount) {
                chargedAmount += projectForce * chargeSpeed * Time.deltaTime;
            }
            else {
                chargedAmount = maxChargeAmount;
            }
            float visScale = Mathf.Clamp(chargedAmount/maxChargeAmount, 0, .5F);
            chargeVisual.transform.localScale = new Vector3(visScale,visScale,visScale);
            Debug.Log("Charging: " + chargedAmount);
        }
        else if (canFire && placeholder/*up*/) {
            GameObject newBullet = Instantiate(bullet, muzzle.transform.position, muzzle.transform.rotation) as GameObject;
            newBullet.GetComponent<Rigidbody>().AddRelativeForce(0, 0, projectForce + chargedAmount);
            Debug.Log("Fired pistol shot with " + (projectForce + chargedAmount) + " charge.");
            chargedAmount = 0;
            canFire = false;
            lastShotTime = Time.time;
            chargeVisual.transform.localScale = Vector3.zero;
            StartCoroutine(cooldown());
        }
        else {
            if (!canFire) {
                mat.SetVector("73ADBC53", new Vector4((Time.time - lastShotTime) / (1 / fireRate),0,0,0)); // doesn't work
            }
        }
    }
}
