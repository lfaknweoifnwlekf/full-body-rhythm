﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


using Valve.VR.InteractionSystem;

public class railgun : MonoBehaviour {

    public GameObject tracer;
    public GameObject muzzle;

    [Tooltip("Shots per second")]
    public float fireRate = 4;
    public float damage = 1;
    public float range = 5000;


    public GameObject test;
    

    private bool canFire = true;
    private float lastShotTime = 0;

    private Material mat;

    // steamvr stuff
    public SteamVR_Action_Single trigger;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;
    public float triggersensitivity = .3F;

    // Use this for initialization
    void Start() {
        mat = GetComponentInChildren<MeshRenderer>().material;
    }

    IEnumerator cooldown() {
        yield return new WaitForSeconds(1 / fireRate);
        canFire = true;
    }

    // Update is called once per frame
    void Update() {
        if(trigger.GetLastAxis(inputSource) < triggersensitivity && trigger.GetAxis(inputSource) > triggersensitivity) {
            firegun();
            
        }
    }

    void firegun() {
        if (canFire) {
            Vector3 hitpos;
            RaycastHit hit;
            
            if (Physics.Raycast(transform.position, muzzle.transform.forward, out hit, range)) {
                hitpos = hit.point;
                target hitEnemy = hit.transform.GetComponent<target>();
                if (hitEnemy != null) {
                    Debug.Log(hitEnemy.points);
                    hitEnemy.damage(damage, hit.point);
                    Debug.Log(hitEnemy.points+1);
                }
            }
            else { // miss
                hitpos = muzzle.transform.position + (muzzle.transform.forward * range);
            }
            // render shot
            GameObject newTrail = Instantiate(tracer, muzzle.transform.position, muzzle.transform.rotation) as GameObject;
            Vector3[] tracerPoints = new Vector3[2];
            tracerPoints[0] = muzzle.transform.position;
            tracerPoints[1] = hitpos;
            newTrail.GetComponent<LineRenderer>().SetPositions(tracerPoints);
            // canfire stuff
            canFire = false;
            lastShotTime = Time.time;
            StartCoroutine(cooldown());
        }
        else {
            mat.SetVector("73ADBC53", new Vector4((Time.time - lastShotTime) / (1 / fireRate), 0, 0, 0)); // doesn't work
        }
    }

    // INput stuff
    //private void OnEnable() {
    //    if (trigger != null) {
    //        trigger.AddOnChangeListener(OnTriggerPressedOrReleased, inputSource);
    //        //trigger.AddOnChangeListener()
    //    }
    //}

    //private void OnDisable() {
    //    if (trigger != null) {
    //        //trigger.RemoveOnChangeListener(OnTriggerPressedOrReleased, inputSource);
    //    }
    //}

    ////private SteamVR_Action_Single.ChangeHandler 

    //private void OnTriggerPressedOrReleased(SteamVR_Action_Single actionIn, SteamVR_Input_Sources inputSource, float newValue) {

    //    Debug.Log("Trigger was pressed or released: "+newValue);
    //    //if (newValue) {
    //    //    firegun();
    //    //}
    //}
}
