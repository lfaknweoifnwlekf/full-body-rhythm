﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnManager : MonoBehaviour {

    public GameObject[] spawnedObjects;
    public float minSpawnDelay = .5F;
    public float maxSpawnDelay = 2;

    public float minSpawnDistance = 2;
    public float maxSpawnDistance = 20;
    public bool flatspawnarea = false;

    //bool canSpawn = true;

	// Use this for initialization
	void Start () {
        StartCoroutine(spawnDelayCountdown());
	}

    void spawn() {
        Vector3 spawnPos = transform.position + Random.insideUnitSphere * Random.Range(minSpawnDistance, maxSpawnDistance);
        if (flatspawnarea) {
            spawnPos.y = transform.position.y;
        }
        Quaternion spawnRot = transform.rotation;
        GameObject newSpawn = Instantiate(spawnedObjects[Random.Range(0,spawnedObjects.Length-1)], spawnPos, spawnRot) as GameObject;
        newSpawn.transform.LookAt(transform.position);
        newSpawn.SetActive(true);
    }

    IEnumerator spawnDelayCountdown() {
        //canSpawn = false;
        yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        spawn();
        StartCoroutine(spawnDelayCountdown());
    }
	
	// Update is called once per frame
	void Update () {
	}
}
