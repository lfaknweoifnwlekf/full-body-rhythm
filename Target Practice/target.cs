﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target : MonoBehaviour {

    
    public GameObject hitEffect;
    public GameObject spawnEffect;

    

    [Header("Stuff")]
    public bool dieOnHit = false;
    public int points = 1;
    public float hp = 1;

    [Header("Velocity (bmombs)")]
    public float minVelocity = 0;
    public float maxVelocity = 0;

    [Header("Self Propelled")]
    [Tooltip("force on spawn, towards player")]
    public float startspeed = 0;
    [Tooltip("force constantly, towards player")]
    public float constantspeed = 0;
    [Tooltip("rotation duration of constantspeed (kunai) objects ()")]
    public float rotspeed = 5;
    public bool damagePlayerOnTouch = false;
    public float damageAmount = 0;

    //[Header("Bomb stuff (spawn on death)")]
    //public GameObject spawnObject;
    //public int spawnObjectCount = 0;
    //public float spawnVelocity = 0;


    private Rigidbody rigid;
    private Collider collid;

    private Transform playertarget;

    private void OnCollisionEnter(Collision collision) {
        // damage player
        collision.transform.SendMessage("damage", damageAmount);
        if (dieOnHit) {
            damage(hp, transform.position);
        }
    }

    // Use this for initialization
    void Start () {
        GameObject newEffect = Instantiate(spawnEffect, transform.position, transform.rotation) as GameObject;
        collid = GetComponent<Collider>();
        // Velocity
        if(maxVelocity > 0) {
            transform.rotation = Random.rotation;
            rigid = GetComponent<Rigidbody>();
            float ranVel = Random.Range(minVelocity, maxVelocity);
            Vector3 velDir = new Vector3(Random.Range(-ranVel / 4, ranVel / 4), ranVel, Random.Range(-ranVel / 4, ranVel / 4));
            rigid.AddForce(velDir);
            rigid.AddTorque(Random.insideUnitSphere * ranVel);
        }
        // self propelled
        if(startspeed != 0 || constantspeed != 0) {
            // targeting player
            GameObject[] playerlimbs = GameObject.FindGameObjectsWithTag("Player");
            playertarget = playerlimbs[Random.Range(0,playerlimbs.Length-1)].transform;
            Debug.Log(playertarget);
            // start force
            transform.LookAt(playertarget);
            rigid = GetComponent<Rigidbody>();
            rigid.AddRelativeForce(Vector3.forward * startspeed);
            //rigid.AddTorque(new Vector3(startspeed / 10, 0, 0));
        }
    }

    public void damage(float amount, Vector3 hitpos) {
        collid.enabled = false;
        GameObject HitEffect = Instantiate(hitEffect, hitpos, transform.rotation) as GameObject;
        //if (spawnObjectCount > 0) {
        //    for (int i = 0; i < spawnObjectCount; i++) {
        //        GameObject spawnobj = Instantiate(spawnObject, transform.position + Random.insideUnitSphere, Random.rotation) as GameObject;
        //    }
        //}
        hp -= amount;
        if (hp <= 0) {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (constantspeed != 0) {
            //transform.LookAt(playertarget);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation((playertarget.position-transform.position).normalized), Time.deltaTime*rotspeed);
            rigid.AddRelativeForce(Vector3.forward * constantspeed *Time.deltaTime);
        }
	}
}
