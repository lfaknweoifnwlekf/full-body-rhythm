﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class childCollisionEnterFix : MonoBehaviour {

    private Enemy parent;
    private Rigidbody rigid;
    private Vector3 velocity;

    private void OnCollisionEnter(Collision collision) {
        parent.receiveForce(collision, rigid.mass, (velocity - rigid.velocity).magnitude, gameObject);
    }

    // Use this for initialization
    void Start () {
        parent = GetComponentInParent<Enemy>();
        rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        velocity = rigid.velocity; // this could be moved to a coroutine each second if lag
	}
}
