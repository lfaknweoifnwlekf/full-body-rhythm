﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class controllerGeneric : MonoBehaviour
{

    [Header("Refernces")]
    public LineRenderer laserVisual;
    public LayerMask laserMask;
    public LayerMask laserMaskMenuOnly;
    protected int laserVisColor;
    [HideInInspector] public Animator anim;
    protected Transform laserMuzzle;
    protected fullBodyAssign fbtMgr;
    protected Rigidbody rigid;

    public Transform hand;
    public Transform foot;
    public Transform hip;

    // refers to songManager's blockPrefabs array
    public bool isPauseMenu = false;
    public bool isPauseMenuPointer = false;

    Vector3 playerCenter;

    // steamvr stuff
    [Header("SteamVR INput")]
    protected SteamVR_Behaviour_Pose trackedObj;
    public SteamVR_Action_Boolean button1 = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Button1");
    public SteamVR_Action_Boolean button2 = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Button2");
    public SteamVR_Action_Boolean menu = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");
    public SteamVR_Action_Boolean padTouch = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("touchpadtouch");
    public SteamVR_Action_Boolean padClick = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("touchpadclick");
    public SteamVR_Action_Vector2 padPos = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("touchpadposition");
    public bool reverseTouchpad = false;
    protected int revPad = 1; // or -1

    protected bool padClickedDown = false;
    protected bool padTouched = false;
    public bool button1Down = false;

    // this needs to be genericized into game-specifyable zones, math todo
    protected bool padState = false;
    protected Vector2 padDir;
    protected float padAngle; // -180 to 180 where on the trackpad (for pie segment zones)
    protected float padTouchDistance; // distance from center of pad (for deadzone)

    // Start is called before the first frame update
    public virtual void genericStart()
    {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
        anim = GetComponentInChildren<Animator>();
        laserMuzzle = laserVisual.transform;
        playerCenter = new Vector3(0, 1, 0);
        fbtMgr = GetComponentInParent<fullBodyAssign>();

        rigid = transform.parent.GetComponent<Rigidbody>();

        if (reverseTouchpad) {
            revPad = -1;
        }
        
    }

    public bool isbutton1Down() {
        return button1.GetState(trackedObj.inputSource);
    }

    protected virtual void Update()
    {
        // get pad touch position as angle. the rest is handled by child class
        if (padTouch.GetState(trackedObj.inputSource))
        {
            padDir = padPos.GetAxis(trackedObj.inputSource) * new Vector2(revPad,1);
            padClickedDown = padClick.GetStateDown(trackedObj.inputSource);
            padTouched = true;
            padTouchDistance = Vector2.Distance(Vector2.zero, padDir);
            //padAngle = Vector2.SignedAngle(Vector2.zero, padDir);
            padAngle = Vector2.SignedAngle(new Vector2(0,0.00001F), padDir);
            padState = true;
        }
        else
        {
            padTouched = false;
            padClickedDown = false;
        }
    }
}
