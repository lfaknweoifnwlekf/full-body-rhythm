﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

/* Tries to figure out which full-body tracker is on which part of body (feet, hips) */

public class fullBodyAssign : MonoBehaviour
{
    public bool autoShuffleTrackers = true;

    [Header("Settings")]
    public bool footLocomotion = false;

    [Header("Trackers")]
    public controllerGeneric[] hands;
    public SteamVR_TrackedObject[] trackers;
    public Transform testTrackerContainer;
    private SteamVR_TrackedObject[] testTrackers;
    public SteamVR_TrackedObject hipObj;
    public Transform hipDir;
    public GameObject leftHand;
    public GameObject rightHand;
    private controllerGeneric leftHandTrack;
    private controllerGeneric rightHandTrack;
    public GameObject applyFBTButton;
    public Rigidbody rigid;
    [SerializeField] private Transform roomCenterVisual;
    
    public GameObject head;
    [HideInInspector] public Camera headCamera;
    private Transform playspace;
    public float maxDistanceToAssignTracker = .2F;

    [Header("Input")]
    public SteamVR_Action_Boolean button1 = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");

    public bool isTyping = false; // prevent UI focus changes while typing for convenience

    Vector3 startWorldPosition;

    // Start is called before the first frame update
    void Start()
    {
        playspace = transform;
        startWorldPosition = transform.position;
        leftHandTrack = leftHand.GetComponent<controllerGeneric>();
        rightHandTrack = rightHand.GetComponent<controllerGeneric>();
        testTrackers =  testTrackerContainer.GetComponentsInChildren<SteamVR_TrackedObject>();
        headCamera = head.GetComponent<Camera>();
        //Debug.Log(leftHandTrack);
    }

    // Update is called once per frame
    void Update()
    {
        // Apply tracker assignments
        if (autoShuffleTrackers && leftHandTrack.isbutton1Down() && rightHandTrack.isbutton1Down())
        {
            applyFBTOffsets();
        }
        // Display tracker assignments in realtime
        if (autoShuffleTrackers)
        {
            foreach (SteamVR_TrackedObject trackerTest in testTrackers)
            {
                if (Vector3.Distance(trackerTest.transform.position, hands[0].transform.position) > .05F && // prevent head/hands from being assigned
                    Vector3.Distance(trackerTest.transform.position, hands[1].transform.position) > .05F &&
                    Vector3.Distance(trackerTest.transform.position, head.transform.position) > .05F)
                {
                    float closest = 1000;
                    GameObject closestTracker = trackerTest.gameObject;
                    foreach (SteamVR_TrackedObject tracker in trackers)
                    {
                        if (Vector3.Distance(trackerTest.transform.position, tracker.transform.position) < closest)
                        {
                            closest = Vector3.Distance(trackerTest.transform.position, tracker.transform.position);
                            closestTracker = tracker.gameObject;
                        }
                    }
                    LineRenderer lr = trackerTest.transform.GetComponent<LineRenderer>();
                    if (closest < maxDistanceToAssignTracker)
                    {
                        lr.enabled = true;
                        lr.SetPosition(0, trackerTest.transform.position);
                        lr.SetPosition(1, closestTracker.transform.position);
                    }
                    else lr.enabled = false;
                }
            }
        }

        /* Foot-movement-driven locomotion */
        if (footLocomotion)
        {

        }

    }

    public void applyFBTOffsets()
    {
        autoShuffleTrackers = false;
        
        
        foreach (SteamVR_TrackedObject trackerTest in testTrackers)
        {
            if (Vector3.Distance(trackerTest.transform.position, hands[0].transform.position) > .05F && // prevent head/hands from being assigned
                Vector3.Distance(trackerTest.transform.position, hands[1].transform.position) > .05F &&
                Vector3.Distance(trackerTest.transform.position, head.transform.position) > .05F)
            {
                // find the closest player limb to each steamvr trackedobject
                float closest = 1000;
                GameObject closestTracker = trackerTest.gameObject;
                foreach (SteamVR_TrackedObject tracker in trackers)
                {
                    if (Vector3.Distance(trackerTest.transform.position, tracker.transform.position) < closest)
                    {
                        closest = Vector3.Distance(trackerTest.transform.position, tracker.transform.position);
                        closestTracker = tracker.gameObject;
                    }
                }
                // if it's close enough, pair to the player limb
                if (closest < maxDistanceToAssignTracker)
                {                
                    GameObject closestTrackerVisual = closestTracker.GetComponentInChildren<Renderer>().gameObject;                    
                    closestTrackerVisual.transform.parent = playspace;
                    if (closestTracker == hipObj.gameObject) closestTrackerVisual.transform.Translate(new Vector3(0, (trackerTest.transform.position.y - closestTrackerVisual.transform.position.y), 0),Space.World); // raise/lower hip to match player's hip height (other height differences irrelavent, scale everything later)
                    closestTracker.transform.SetPositionAndRotation(trackerTest.transform.position, trackerTest.transform.rotation);
                    closestTracker.GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)trackerTest.index);
                    closestTrackerVisual.transform.parent = closestTracker.transform;
                    closestTracker.GetComponent<trackerPower>().applyTracker();
                }
                // scale player to standardized height based on head height (so short players can  reach blocks placed by tall players etc)
                if (head.transform.position.y > 0) {
                    transform.localScale *= 1.8F / head.transform.position.y;
                }
            }
        }
        applyFBTButton.SetActive(false);
        testTrackerContainer.gameObject.SetActive(false);
        if (rigid != null) rigid.isKinematic = false;
    }

    public void resetFBTOffsets()
    {
        if (rigid != null) rigid.isKinematic = true;
        foreach (SteamVR_TrackedObject tracker in trackers) tracker.GetComponent<trackerPower>().resetTracker(head.transform.position);
        foreach (SteamVR_TrackedObject trackerTest in testTrackers) trackerTest.gameObject.SetActive(true);
        autoShuffleTrackers = true;
        applyFBTButton.SetActive(true);
        testTrackerContainer.gameObject.SetActive(true);
    }

    public void resetRoomCenterAndRotation() {
        Vector3 offset = head.transform.position - transform.position;
        Vector3 offsetFromCenter = head.transform.position - startWorldPosition;
        offset.y = 0;
        offsetFromCenter.y = 0;
        transform.position -= offsetFromCenter;
        transform.RotateAround(head.transform.position, Vector3.up, -head.transform.eulerAngles.y);
        //transform.rotation = Quaternion.LookRotation(hipDir.position - hipObj.transform.position, Vector3.up);
        roomCenterVisual.position = new Vector3(head.transform.position.x, roomCenterVisual.position.y, head.transform.position.z);
        foreach (SteamVR_TrackedObject tracker in trackers){
            //trackerPower trackerP = tracker.GetComponent<trackerPower>();
            //trackerP.spawnPos = trackerP.spawnOffset + offset;
            //trackerP.resetTracker();
        }
    }
}
