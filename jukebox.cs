﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class jukebox : MonoBehaviour
{


    //	public Texture2D customCursor;
    public AudioClip[] playlist;

    // options
    AudioSource musicSource;
    int currentSong = -1;
    private float musicVolume = 1;
    private bool paused = true;

    //	GameObject player;
    // Use this for initialization
    void Start()
    {
        musicSource = GetComponent<AudioSource>();
        StartCoroutine(checkIfPlaying());
    }


    public void startMusic(float delay)
    {

    }

    IEnumerator startMusicDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        musicSource.Play();
    }

    IEnumerator checkIfPlaying()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);

            if (!paused && !musicSource.isPlaying)
            { //play random song if last song is over, will not repeat same song
            }
        }
    }

    private void trackForward()
    {
        int randomSong = Random.Range(0, playlist.Length);
        while (randomSong == currentSong)
        { // no repeat songs
            randomSong = Random.Range(0, playlist.Length);
        }
        musicSource.clip = playlist[randomSong];
        currentSong = randomSong;
        musicSource.Play();
    }
    
    public void setMusicMute(bool f)
    {
        if (f)
        {
            musicSource.volume = 0;
        }
        else
        {
            musicSource.volume = musicVolume;
        }
    }

    public void setMusicVolume(float f)
    {
        musicVolume = f;
        musicSource.volume = f;
    }
}
