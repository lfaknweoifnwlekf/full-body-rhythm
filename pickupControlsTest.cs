﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

using Valve.VR.InteractionSystem;

public class pickupControlsTest : MonoBehaviour {

    public GameObject spawnable;


    public SteamVR_Action_Boolean button1;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable() {
        if (button1 != null) {
            button1.AddOnChangeListener(OnTriggerPressedOrReleased, inputSource);
        }
    }

    private void OnDisable() {
        if (button1 != null) {
            button1.RemoveOnChangeListener(OnTriggerPressedOrReleased, inputSource);
        }
    }

    private void OnTriggerPressedOrReleased(SteamVR_Action_Boolean actionIn, SteamVR_Input_Sources inputSource, bool newValue) {

        Debug.Log("button1 was pressed or released");
        GameObject newbox = Instantiate(spawnable, Vector3.zero, Quaternion.Euler(Vector3.zero));
    }
}
