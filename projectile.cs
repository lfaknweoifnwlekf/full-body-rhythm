﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour {

    public float damage = 1;

    private Rigidbody rigid;

    private void OnCollisionEnter(Collision collision) {
        onImpact(collision.transform);
    }

    private void OnTriggerEnter(Collider other) {
        onImpact(other.transform);
        
    }

    void onImpact(Transform other) {
        //Debug.Log("Projectile " + transform.name + " just hit " + other.name);
        target hitEnemy = other.GetComponent<target>();
        if (hitEnemy != null) {
            hitEnemy.damage(damage, transform.position);
            //hitEnemy.damage(1, collision.contacts[0].point);
        }
        Destroy(gameObject);
    }

   // Use this for initialization
   void Start () {
        rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // raycast check (sometimes colliders pass through)
    private void FixedUpdate() {
        RaycastHit hit;
        Debug.DrawRay(transform.position, rigid.velocity * rigid.velocity.magnitude * Time.deltaTime, Color.cyan, 5);
        if (Physics.Raycast(transform.position,rigid.velocity, out hit, rigid.velocity.magnitude * Time.deltaTime *2)) {
            //
            onImpact(hit.transform);
        }
    }
}
