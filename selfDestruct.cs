﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selfDestruct : MonoBehaviour {

    public float lifetime = 1;
	[SerializeField] private bool justDisable = false;

    private void OnEnable()
    {
		StartCoroutine(decay());
	}
    

	IEnumerator decay() {
        yield return new WaitForSeconds(lifetime);
		if (justDisable) gameObject.SetActive(false);
        else Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
